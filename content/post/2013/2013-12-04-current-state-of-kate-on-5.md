---
title: Current State of Kate on 5
author: Dominik Haumann

date: 2013-12-03T23:14:14+00:00
url: /2013/12/04/current-state-of-kate-on-5/
pw_single_layout:
  - "1"
categories:
  - Developers
  - Users
tags:
  - planet

---
Just a quick update on the frameworks 5 port: KTextEditor,  Kate Part and KWrite build and run. Kate is not yet ported and does not compile. These changes were already committed:

  * made KTE::Cursor and KTE::Range non-virtual for maximum speed
  * removed KTE::EditorChooser, since Kate Part is the only implementation since over 10 years. Just use KTE::Editor* editor = KTE::editor(); to get KatePart.
  * new signal KTE::Document::readWriteChanged()
  * removed KTE::LoadSaveFilterCheckPlugin, was unused since years
  * removed all KTE::Smart\* classes in favor of KTE::Moving\* classes
  * merged KTE::CoordinatesToCursorInterface into KTE::View
  * new: KTE::Range KTE::Document::wordRangeAt(KTE::Cursor)
  * new: QString KTE::Document::wordAt(KTE::Cursor)
  * new: KTE::DocumentCursor
  * ported lots of KSharedPtr to QSharedData
  * updated Mainpage.dox to reflect porting notes
  * some small interface cleanups and additions

Please feel free to join the porting fun. Just follow the <a title="Build KDE Frameworks" href="http://community.kde.org/Frameworks/Building" target="_blank">Frameworks compile guide</a> and checkout the git branch &#8220;frameworks&#8221; in the Kate git module.