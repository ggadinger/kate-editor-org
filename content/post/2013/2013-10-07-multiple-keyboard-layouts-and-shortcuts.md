---
title: Multiple Keyboard Layouts and Shortcuts
author: Dominik Haumann

date: 2013-10-07T16:21:47+00:00
url: /2013/10/07/multiple-keyboard-layouts-and-shortcuts/
pw_single_layout:
  - "1"
categories:
  - Common
  - KDE
  - Users
tags:
  - planet

---
KDE has a very handy feature to switch keyboard layouts on the fly. This is for instance useful if you use the German keyboard layout by default, and the US layout for programming. To enable keyboard layout switching, go into System Settings > Input Devices (category Hardware) > Keyboard item > Layouts tab:

<img class="aligncenter size-full wp-image-2879" title="Keyboard Layout Settings" src="/wp-content/uploads/2013/10/keyboard-layouts.png" alt="" width="741" height="620" srcset="/wp-content/uploads/2013/10/keyboard-layouts.png 741w, /wp-content/uploads/2013/10/keyboard-layouts-300x251.png 300w" sizes="(max-width: 741px) 100vw, 741px" /> 

Here, &#8216;**[x] Configure layouts**&#8216; is checked, enabling the list view. I added the German keyboard layout, and then the English (US) layout. Notice also, that the shortcut is set to &#8216;**Ctrl+Alt+K**&#8216;. Clicking apply, a tiny little indicator appears in the panel:

[<img class="aligncenter size-full wp-image-2880" title="Switching Keyboard Layouts in the Panel" src="/wp-content/uploads/2013/10/keyboard-layout-panel.png" alt="" width="303" height="108" srcset="/wp-content/uploads/2013/10/keyboard-layout-panel.png 303w, /wp-content/uploads/2013/10/keyboard-layout-panel-300x106.png 300w" sizes="(max-width: 303px) 100vw, 303px" />][1] You now can quickly switch with Ctrl+Alt+K between the German and the US layout. Quite efficient, especially since the keyboard layout config page allows to switch the language on application basis.

### Unchanged Keyboard Shortcuts

Switching the keyboard layout has one potential issue, though: The shortcuts remain unchanged. So if undo is mapped to _Ctrl+z_ in the German layout, it is still mapped to _Ctrl+z_ in the US layout. Note that by &#8216;z&#8217; we refer to the hardware key on the keyboard. As a consequence, in the US layout, hitting the hardware key &#8216;y&#8217; on the German keyboard inserts the character &#8216;z&#8217;, but the z in _Ctrl+z_ is still on the hardware key &#8216;z&#8217;. This behavior may or may not be wanted.

Getting more into detail reveals that the order of the keyboard layouts in the first screen shot is of importance: If you first add the German &#8216;_de_&#8216; layout, and then the English &#8216;_us_&#8216; layout, then the shortcuts will always use the Germany keyboard layout, independent of what keyboard layout is chosen.

Reversely, if you first add the English &#8216;us&#8217; layout, and then the German &#8216;_de_&#8216; layout, then the shortcuts will always use the English &#8216;_us_&#8216; keyboard layout.

So it seems that the order defines a priority, and the shortcuts always use the first entry in the list.

The correct solution to fix this would (in my humble opinion) be to add an option &#8216;**[x] Shorcuts follow keyboard layout**&#8216; or similar. But since this option does not exist, let&#8217;s do a quick hack to still get what we want here.

### A Workaround

First we reset the shortcut in the settings of the keyboard layout options:

<img class="aligncenter size-full wp-image-2881" title="Reset Keyboard Shortcut to Switch Keyboard Layouts" src="/wp-content/uploads/2013/10/keyboard-layout-no-shortcut.png" alt="" width="266" height="172" /> 

Click apply and close the dialog. Now, the shortcut &#8216;**Ctrl+Alt+K**&#8216; is unbound. Our idea is now to create a script that toggles the keyboard layout by calling **setxkbmap** with the appropriate parameters and bind this script via a global shortcut to &#8216;**Ctrl+Alt+K**&#8216;.

To this end, we first have to create the script. So let&#8217;s first type \`**setxkbmap -query**\` in the console and check the output. For me, this results in:

<pre style="padding-left: 30px;">$ setxkbmap -query
rules: evdev
model: pc101
layout: de,us
variant: nodeadkeys,</pre>

From this, we can follow that the current xkb layout is achieved with:

<pre style="padding-left: 30px;">setxkbmap -model pc101 -layout de,us -variant nodeadkeys</pre>

Now, let&#8217;s switch the **de,us** to **us,de** and try the following:

<pre style="padding-left: 30px;">setxkbmap -model pc101 -layout us,de -variant nodeadkeys</pre>

Notice, that the keyboard layout indicator in the panel switched to &#8216;**us**&#8216;. Calling the first variant with **de,us** again, we get back to the German layout.

This discovery leads us to the following script **switch-keyboard-layout.sh**:

<pre style="padding-left: 30px;">#!/bin/sh

# query xkb map: us,de -&gt; us is primary; de,us -&gt; de is primary
dummy=`setxkbmap -query | grep us,de`

# return value 0: us,de; return value != 0, de,us
if [ $? -ne 0 ]; then
  # de is primary, now make us primary in list
  setxkbmap -model pc101 -layout us,de -variant nodeadkeys
else
  # us is primary, now make de primary in list
  setxkbmap -model pc101 -layout de,us -variant nodeadkeys
fi</pre>

Save this script somewhere to **switch-keyboard-layout.sh** and make it executable with

<pre style="padding-left: 30px;">chmod 755 switch-keyboard-layout.sh</pre>

Each time we execute this script, the keyboard layout is toggled.

Next, we go into System Settings again and navigate to Shortcuts and Gestures (Common Appearance and Behavior), there in the Custom Shortcuts we add a new Command/URL item named &#8216;SwitchKeyboardLayout&#8217; as follows:

<img class="aligncenter size-full wp-image-2885" title="Create global shortcut to switch Keyboard Layout" src="/wp-content/uploads/2013/10/global-shortcut.png" alt="" width="663" height="426" srcset="/wp-content/uploads/2013/10/global-shortcut.png 663w, /wp-content/uploads/2013/10/global-shortcut-300x192.png 300w" sizes="(max-width: 663px) 100vw, 663px" /> 

As a comment for this new item, we write &#8216;**Switch Keyboard Layout**&#8216;, in the &#8216;**Trigger**&#8216; tab, bind the global shortcut to &#8216;**Ctrl+Alt+K**&#8216;, and in the &#8216;**Action**&#8216; tab, choose the **switch-keyboard-layout.sh** script. Finally click **Apply**, and close the dialog.

Now, hitting Ctrl+Alt+K calls our script and correctly toggles the keyboard layout _including_ the shortcuts.

Unfortunately, this approach does not support e.g. switching the keyboard layout on application basis as the switching policy of the Keyboard settings (first screen shot) allows. Still it works.

A final remark, though: For GTK applications this works out of the box. So is there any real reason why this is not the case for KDE / Qt applications? A real fix would be very much appreciated, I&#8217;d be also fine with an option. But not providing this feature at all is very thin ice&#8230;

**Update**: This issue was reported as KDE <a title=" Shortcuts ignore keyboard layout" href="https://bugs.kde.org/show_bug.cgi?id=197552" target="_blank">bug #197552</a> in 2009, and resolved as an upstream issue. However, it never was reported to Qt upstream. If I may say so, this is not how resolving bugs in KDE usually works. Grrr&#8230;

 [1]: /wp-content/uploads/2013/10/keyboard-layout-panel.png