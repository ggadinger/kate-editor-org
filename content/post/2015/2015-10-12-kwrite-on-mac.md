---
title: KWrite on Mac
author: Christoph Cullmann

date: 2015-10-12T21:36:24+00:00
url: /2015/10/12/kwrite-on-mac/
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
After some hacking on frameworks, I have the first running KWrite version on Mac that uses an unpatched Qt from the qt.io offline installer:  
[<img class="aligncenter size-medium wp-image-3657" src="/wp-content/uploads/2015/10/kwrite-300x245.png" alt="KWrite on Mac" width="300" height="245" srcset="/wp-content/uploads/2015/10/kwrite-300x245.png 300w, /wp-content/uploads/2015/10/kwrite-1024x836.png 1024w, /wp-content/uploads/2015/10/kwrite.png 1504w" sizes="(max-width: 300px) 100vw, 300px" />][1]  
It is still ugly, as scaled on my HiDPI display as the plist file is missing and it crashs on everything (aka open dialog) and has no icons.

But beside that, it works.

No magic, just the xcode clang toolchain + stock qt + stock cmake + gettext + KDE Frameworks 5 ;)

Script that did the job (still some patches to frameworks missing in master that remove optional deps and fixes linking), it is available in updated form in kate.git:

https://quickgit.kde.org/?p=kate.git&a=blob&f=mac.txt

The changes will benefit Windows builds, too, as they made the dependencies a bit smaller. You for example need no longer phonon to get this running, nor do you need to hassle around with the xslt/xml doctools on Windows.

Patches that are needed t to be applied to have this working:

https://git.reviewboard.kde.org/r/125614/

https://git.reviewboard.kde.org/r/125616/

 [1]: /wp-content/uploads/2015/10/kwrite.png