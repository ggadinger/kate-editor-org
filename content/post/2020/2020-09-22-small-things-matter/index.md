---
title: KTextEditor - Small Things Matter
author: Christoph Cullmann
date: 2020-09-22T21:51:00+02:00
url: /post/2020/2020-09-22-small-things-matter/
---

Thanks to the feedback & patches provided by others, I found a bit more motivation to take a look at the small things that seems to be odd in KTextEditor.

Interesting enough, if you once notice a small detail (like a dead pixel on your display you suddenly find after years of use), it really sticks out like a sore thumb...

Here two small things that caught my interest this week.

### KTextEditor border widget painting

If you have some theme like "Vim Dark" with border background color equal to the text background color, strange stuff happens.

See the following screenshot:

<p align="center">
    <a href="/post/2020/2020-09-22-small-things-matter/images/ktexteditor-old-border.png" target="_blank"><img width=700 src="/post/2020/2020-09-22-small-things-matter/images/ktexteditor-old-border.png"></a>
</p>

You can't actually tell: Is this the folding marker column or is all my text indented by one or two spaces?
There is no visual hint for this.

Funny enough, there is some visual hint to show that on the left side you might have some marks, if you turn that on in addition:

<p align="center">
    <a href="/post/2020/2020-09-22-small-things-matter/images/ktexteditor-old-border-with-marks.png" target="_blank"><img width=700 src="/post/2020/2020-09-22-small-things-matter/images/ktexteditor-old-border-with-marks.png"></a>
</p>

But then you see the next strange stuff: this separator is only painted until the last line of text is reached, strange, too.
I think this behavior was like this since we implemented that parts of the rendering.

I never noticed that until I tried out to use the new nifty ayu theme as my daily theme (see [the themes gallery](/themes)).

Now, I scratched that itch in [this merge request](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/28).

The current state with Vim Dark looks now like this (will be in Frameworks 5.75):

<p align="center">
    <a href="/post/2020/2020-09-22-small-things-matter/images/ktexteditor-new-border-with-marks.png" target="_blank"><img width=700 src="/post/2020/2020-09-22-small-things-matter/images/ktexteditor-new-border-with-marks.png"></a>
</p>

The separator got just moved between the border widget and the text and the painting got fixed to span the complete visible widget height.

I think this makes both for a better separation between border and text and for a visually better appearance.

Do you need to think about if the text is indented in this screenshot?
I don't think so.

Especially that the old separator got not even drawn for the region below the last line was kind of astonishing, I must confess, that slipped my mind for years until I focused once on that ;=)

You have noticed that the Vim Dark colors did change a bit compared to the above two screenshots?
That's thanks to Nibaldo González scratching an itch, [making the colors more consistent with real Vim](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/50).
Thanks for that!

Just take a look at that perfectly done merge request, you can judge the changes just by taking a look at the before/after screenshots.
Awesome!

### KTextEditor initial input lag

Have you ever noticed that for the first view you type something in, there is a small lag after some keystrokes?
Depending on your machine, it might even be not that short.
And after that initial lag, all is fine.

I noticed that lag already in the past but lost interest to take a look into it, as it just happens once for each start and use of e.g. Kate.

Now that I play around more with short KWrite open, test & close sessions for debugging other stuff, I got more interested again.

It was easy to trace back: on-the-fly spellchecking is the reason for this.

Once at the start, you will initially need to load both the spellchecking dictionary and the trigrams.

Over a lot of different layers, this is triggered after the first few characters are typed (or any other text manipulation).

Given it is non-trivial to remove this lag by moving this to a thread or by other complex solutions I thought about, I went for a quick'n'dirty fix:
just [do that once on startup](https://invent.kde.org/frameworks/ktexteditor/commit/38b87e9fcf5fe424ffc70f5b0fa05e324e3879f7) if we have on-the-fly spellchecking enabled.

Some fractions of a second on startup more is much nicer UI wise than a short hang during your initial typing that kills your flow.

With this change, Frameworks 5.75 will have lag free input even directly after the startup of your KTextEditor based application.

And for people that have no on-the-fly spellchecking enabled, no extra penalty is there.

### Done ;) For now...

I hope this small improvements make some people happy (or at least don't annoy them).

If you happen to see such small issues and you can code, please think about [contributing](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/) some patch to fix them.

Even fixing such small things can have a large impact on the usability and perception of our stuff.

Help is always welcome!
