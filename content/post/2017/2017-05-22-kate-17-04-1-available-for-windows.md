---
title: Kate 17.04.1 available for Windows
author: Kåre Särs

date: 2017-05-22T19:03:48+00:00
url: /2017/05/22/kate-17-04-1-available-for-windows/
categories:
  - Common

---
### **<span style="color: #ff0000;">NOTE:</span> Please refer to [Get It](/get-it/) for up-to-date Windows versions**

Installers for Kate 17.04.1 are now available for download!

This release includes, besides bug-fixing and features, an update to the search in files plugin. The search-while-you-type in the current file should not &#8220;destroy&#8221; your last search in files results as easily as previously. The search-combo-box-history handling is also improved.

Grab it now at download.kde.org:  [Kate-setup-17.04.1-KF5.34-32bit][1] or [Kate-setup-17.04.1-KF5.34-64bit][2]

 [1]: https://download.kde.org/stable/kate/Kate-setup-17.04.1-KF5.34-32bit.exe
 [2]: https://download.kde.org/stable/kate/Kate-setup-17.04.1-KF5.34-64bit.exe
