---
title: Improved PHP support in Kate
author: Milian Wolff

date: 2009-08-26T14:20:49+00:00
excerpt: 'Not only KDevelop gets better and better PHP support &#8212; the Kate PHP syntax file also got a few new features and fixes over the last weeks. The good thing is of course that all users of KWrite, Kate, Quanta, KDevelop and other editors leveraging t...'
url: /2009/08/26/improved-php-support-in-kate/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
"rss:comments":
  - 'http://milianw.de/blog/improved-php-support-in-kate#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/102
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/improved-php-support-in-kate
syndication_item_hash:
  - 34c1c30cd44edb4fb1da1b1351603ba3
categories:
  - Users

---
Not only KDevelop gets better and better PHP support &#8212; the Kate PHP syntax file also got a few new features and fixes over the last weeks. The good thing is of course that all users of KWrite, Kate, Quanta, KDevelop and other editors leveraging the Katepart benefit from these changes.

###### Improved HereDocs

<p style="float:right; text-align:center; font-size:xx-small; margin-left:10px;">
  <a href="http://imagebin.ca/view/YOWiEyE.html" title="view fullsized screenshot"> <img alt="screenshot of improved highlighting in PHP heredocs" src="http://imagebin.ca/img/YOWiEyE.png" height="200" /> <br /> screenshot of improved highlighting in PHP heredocs </a>
</p>

I went over [PHP related bugs][1] on [bugs.kde.org][2] today and spotted one that was fairly easy to fix:

> [vim-like syntax highlighting support for heredocs in php.xml][3]

With some magic (<span class="geshifilter"><code class="text geshifilter-text">IncludeRules</code></span> just rocks) I got it working fairly easy. You can see the results to the right.

Additionally I added code folding to heredocs, since often these strings include lots of text and hiding it often makes sense.

###### Better support for overlapping syntax regions

<p style="float:left; text-align:center; font-size:xx-small; padding-right:10px;">
  <img alt="code folding with overlapping syntax regions" src="http://i25.tinypic.com/e9gfwi.png" /> <br /> code folding with overlapping syntax regions
</p>

Another long standing bug (_&#8221;[accommodate overlapping syntax regions (especially for php)][4]&#8221;_) got fixed by James Sleeman.

Finally PHP templates with code such as

<div class="geshifilter">
  <pre class="php geshifilter-php" style="font-family:monospace;"><ol>
  <li class="li1">
    <div class="de1">
      <span class="kw2">&lt;?php</span> <span class="kw1">if</span> <span class="br0">&#40;</span> <span class="kw2">true</span> <span class="br0">&#41;</span> <span class="sy0">:</span> <span class="kw2">?&gt;</span>
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      &lt;!-- some html stuff --&gt;
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="kw2">&lt;?php</span> <span class="kw1">elseif</span> <span class="br0">&#40;</span> <span class="kw2">false</span> <span class="br0">&#41;</span> <span class="br0">&#123;</span> <span class="kw2">?&gt;</span>
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      &lt;!-- some other html stuff --&gt;
    </div>
  </li>
  
  <li class="li2">
    <div class="de2">
      <span class="kw2">&lt;?php</span> <span class="br0">&#125;</span> <span class="kw2">?&gt;</span>
    </div>
  </li>
</ol></pre>
</div>

can be folded properly. This kind of spaghetti code is used quite often in simple templates and having the posibility to fold it properly is a huge win in my opinion. Thanks to James Sleeman again!

 [1]: https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=php&product=kate&product=kdevelop&product=kdevplatform&product=quanta&long_desc_type=substring&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&emailassigned_to1=1&emailtype1=substring&email1=&emailassigned_to2=1&emailreporter2=1&emailcc2=1&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&field0-0-0=noop&type0-0-0=noop&value0-0-0=
 [2]: http://bugs.kde.org
 [3]: https://bugs.kde.org/show_bug.cgi?id=118668
 [4]: https://bugs.kde.org/show_bug.cgi?id=103257