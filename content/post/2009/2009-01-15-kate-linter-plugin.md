---
title: Kate linter plugin
author: Milian Wolff

date: 2009-01-15T17:58:24+00:00
excerpt: |
  Just a quicky: I wrote a little plugin for KTextEditor which supplies you with basic error checking when you save documents. Currently only PHP (via php -l) and JavaScript (via JavaScript Lint) are supported.
  
  
  Screenshots
  
  
  PHP error
  list of errors
  
  
  ...
url: /2009/01/15/kate-linter-plugin/
syndication_source:
  - 'Milian Wolff - kate'
syndication_source_uri:
  - http://milianw.de/taxonomy/term/170/0
"rss:comments":
  - 'http://milianw.de/blog/kate-linter-plugin#comments'
"wfw:commentRSS":
  - http://milianw.de/crss/node/89
syndication_feed:
  - http://milianw.de/tag/kate/feed
syndication_feed_id:
  - "8"
syndication_permalink:
  - http://milianw.de/blog/kate-linter-plugin
syndication_item_hash:
  - a3f7ac2c1f3f67f960cd3dcf4b26b441
categories:
  - Developers
  - Users

---
Just a quicky: I wrote a little plugin for KTextEditor which supplies you with basic error checking when you save documents. Currently only PHP (via <span class="geshifilter"><code class="bash geshifilter-bash">php &lt;span class="re5">-l&lt;/span></code></span>) and JavaScript (via [JavaScript Lint][1]) are supported. <!--break-->

##### Screenshots

  * [PHP error][2]
  * [list of errors][3]

##### Requirements

  * usual tools for compiling C++, e.g. gcc.
  * cmake
  * Qt development packages, i.e. under Ubuntu: <span class="geshifilter"><code class="bash geshifilter-bash">&lt;span class="kw2">sudo&lt;/span> &lt;span class="kw2">aptitude&lt;/span> &lt;span class="kw2">install&lt;/span> libqt4-dev</code></span>
  * KDE 4.2 with development packages for kdelibs and kdebase, i.e. under Ubuntu: <span class="geshifilter"><code class="bash geshifilter-bash">&lt;span class="kw2">sudo&lt;/span> &lt;span class="kw2">aptitude&lt;/span> &lt;span class="kw2">install&lt;/span> kdebase-dev kdebase-workspace-dev kdelibs5-dev</code></span>. **Note:** You&#8217;ll need the experimental KDE 4.2 packages activated as of now, see for example [the Kubuntu news on KDE 4.2 RC1][4] for hints.
  * proper setup of environment variables, read [this techbase article][5] for more information. the <span class="geshifilter"><code class="text geshifilter-text">.bashrc</code></span> linked there should be enough for most people
  * For PHP support: a PHP executable which supports the <span class="geshifilter"><code class="text geshifilter-text">-l</code></span> switch for linting
  * For JavaScript support: a JavaScript Lint executable, you could [download][6] and compile the sources for example.

##### Installing

Get [the sources][7] for the linter plugin from KDE SVN and compile it, using e.g. the functions supplied via the <span class="geshifilter"><code class="text geshifilter-text">.bashrc</code></span> mentioned above:

<div class="geshifilter">
  <pre class="bash geshifilter-bash" style="font-family:monospace;"><ol>
  <li class="li1">
    <div class="de1">
      <span class="co0"># go to your development folder</span>
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      cs
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="co0"># checkout sources</span>
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="kw2">svn</span> <span class="kw2">co</span> <span class="kw2">svn</span>:<span class="sy0">//</span>anonsvn.kde.org<span class="sy0">/</span>home<span class="sy0">/</span>kde<span class="sy0">/</span>trunk<span class="sy0">/</span>playground<span class="sy0">/</span>devtools<span class="sy0">/</span>kte_linter
    </div>
  </li>
  
  <li class="li2">
    <div class="de2">
      <span class="kw3">cd</span> kte_linter
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="co0"># build base linter plugin</span>
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="kw3">cd</span> linter
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      cmakekde
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="co0"># build php linter plugin</span>
    </div>
  </li>
  
  <li class="li2">
    <div class="de2">
      <span class="kw3">cd</span> ..<span class="sy0">/</span>phplinter
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      cmakekde
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="co0"># build javascript linter plugin</span>
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="kw3">cd</span> ..<span class="sy0">/</span>jslinter
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      cmakekde
    </div>
  </li>
  
  <li class="li2">
    <div class="de2">
      <span class="co0"># update sycoca</span>
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      kbuildsycoca4
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      <span class="co0"># start editor and select the plugins - happy coding!</span>
    </div>
  </li>
  
  <li class="li1">
    <div class="de1">
      kwrite
    </div>
  </li>
</ol></pre>
</div>

##### Todo

  * Support for more languages
    
    If you know good linters drop me a note. But it would be even better if you could write your own linter plugin. It&#8217;s pretty easy, take a look at one of the existing plugins for a skeleton & documentation.

  * Right now each plugin returns a hardcoded list of highlighting-modes which it supports for linting. This should be made configurable so that custom highlighting modes are supported

  * make error messages more pretty

Happy coding!

 [1]: http://javascriptlint.com/
 [2]: http://milianw.de/files/screens/kte_linter/php_error.png
 [3]: http://milianw.de/files/screens/kte_linter/error_list.png
 [4]: http://www.kubuntu.org/news/kde-4.2-rc1
 [5]: http://techbase.kde.org/Getting_Started/Increased_Productivity_in_KDE4_with_Scripts#Bash
 [6]: http://javascriptlint.com/download.htm
 [7]: http://websvn.kde.org/trunk/playground/devtools/kte_linter/