---
title: Kate in KDE 3.5.4
author: Dominik Haumann

date: 2006-08-02T18:51:00+00:00
url: /2006/08/02/kate-in-kde-3-5-4/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2006/08/kate-in-kde-354.html
categories:
  - Common

---
Finally, [KDE 3.5.4 is out][1]! We were able to [fix a lot of issues in Kate][2] and the [bug curves went noticeably down][3]. Thanks to all contributors for the nice KDE release :)

 [1]: http://www.kde.org/announcements/announce-3.5.4.php
 [2]: http://bugs.kde.org/buglist.cgi?&product=kate&resolution=FIXED&resolution=WORKSFORME&chfield=resolution&chfieldfrom=2006-05-25&chfieldto=2006-07-24
 [3]: http://bugs.kde.org/reports.cgi?product=kate&output=show_chart&datasets=NEW%3A&datasets=ASSIGNED%3A&datasets=REOPENED%3A&datasets=UNCONFIRMED%3A&links=1&banner=1&quip=0