---
title: What happened to Kate in Randa?
author: Kåre Särs

date: 2016-07-08T19:52:21+00:00
url: /2016/07/08/what-happened-to-kate-in-randa/
categories:
  - Common

---
This years topic for the Randa meeting, was multi-platform end-user application development. That was a golden opportunity to work on the Windows and Mac versions.

One thing that happened was that the icon.rcc file generation is moved to the breeze-icons repository in stead of doing it separately for every application. The icon.rcc file loading was moved to KIconThemes so that all applications linking to it don&#8217;t have to invent the wheel again. This would not have been so easy if we hadn&#8217;t been at the same place discussing things face to face.

A more noticeable thing is that Kate now does not need a DBus server on Windows and Apple any more. Both Kate and KDevelop now use QtSingleApplication to open all documents in one window.

The search plugin also got a face-lift. Hopefully the new layout will be a bit more useful than the previous :)

A Randa development release of Kate on Windows was also done. Download the installer from [here][1].

To be able to continue with similar activities we [need your help.][2]

[<img class="aligncenter" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" />][3]

 [1]: http://download.kde.org/unstable/kate/
 [2]: https://www.kde.org/fundraisers/randameetings2016/
 [3]: https://www.kde.org/fundraisers/randameetings2016