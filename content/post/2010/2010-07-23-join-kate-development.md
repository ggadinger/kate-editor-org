---
title: Join Kate Development!
author: Dominik Haumann

date: 2010-07-23T17:10:53+00:00
url: /2010/07/23/join-kate-development/
categories:
  - Developers
tags:
  - planet

---
<p style="text-align: left;">
  The Wheel of Time turns&#8230; meaning that the Kate Project has quite along history by now. The Kate Project was started back in December 2000, so it&#8217;s almost 10 years old. Development sometimes continues with a fast pace; and at other times there is almost no progress for weeks. But all in all, looking back at those 10 years, we can proudly tell you that the project is very much alive. Let&#8217;s take a look at the <em>traffic of our mailing</em> list:<img class="size-full wp-image-459 aligncenter" title="Traffic on the kate mailing list" src="/wp-content/uploads/2010/07/kwrite-devel.png" alt="" width="440" height="221" srcset="/wp-content/uploads/2010/07/kwrite-devel.png 440w, /wp-content/uploads/2010/07/kwrite-devel-300x150.png 300w" sizes="(max-width: 440px) 100vw, 440px" />
</p>

<p style="text-align: left;">
  The traffic itself does not say much about the development. It probably tells us that there are quite a lot of people using Kate and reporting wishes and bugs. This is a good thing :) The commit statistics for Kate, KWrite and the KTextEditor interfaces look like this:
</p>

<p style="text-align: left;">
  <img class="aligncenter size-full wp-image-460" title="Kate, KWrite and KTextEditor Commits" src="/wp-content/uploads/2010/07/kate-commits.png" alt="" width="440" height="221" srcset="/wp-content/uploads/2010/07/kate-commits.png 440w, /wp-content/uploads/2010/07/kate-commits-300x150.png 300w" sizes="(max-width: 440px) 100vw, 440px" />Before 2000, Kate did not exist yet. Instead, so these commits come from the KWrite days. The commit rate is quite constant according to this statistic. In fact, there usually are always several developers working on Kate. Some only for a short period to implement single features or fix some bugs, and some long-term contributors. A very nice trend is given by the fact that <a title="The Kate Team" href="/the-team/" target="_self">the core team of Kate grew</a> from about 4 to 7 developers over the last two years. So the Kate Project is very healthy &#8211; nice! And of course we are happy for every single contribution. So if you want to get involved in Kate development, <a title="Join Us!" href="/join-us/" target="_self">join us</a>!
</p>