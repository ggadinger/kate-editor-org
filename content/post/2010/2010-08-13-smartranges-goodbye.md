---
title: SmartRanges goodbye
author: Christoph Cullmann

date: 2010-08-13T06:53:13+00:00
url: /2010/08/13/smartranges-goodbye/
categories:
  - Developers
tags:
  - planet

---
Like told in previous posts [here][1] and [there][2], KDE 4.6 won&#8217;t have a KatePart that supports the SmartInterface but instead KatePart moves on to the new and shiny (and much simpler) MovingInterface for the handling of automatic moving cursors and ranges.

Now the big cut is done: KatePart in /trunk no longer implements the SmartInterface. KDevelop already adopted the new interface in a development branch, lets see how that all evolves.

<pre>SVN commit 1162946 by cullmann:

Smart* handling is purged in kate part now for KDE 4.6.
It seems to work, at least I used it in that state now more than a day.
More stuff needs polishing still, like removing the duplicated edit history, but nice first start.


 M  +0 -7      CMakeLists.txt  
 M  +0 -1      completion/katecompletiondelegate.cpp  
 M  +13 -49    completion/katecompletionwidget.cpp  
 M  +5 -6      completion/katecompletionwidget.h  
 M  +6 -215    document/katedocument.cpp  
 M  +0 -64     document/katedocument.h  
 M  +1 -5      render/katerenderer.cpp  
 M  +0 -191    render/katerenderrange.cpp  
 M  +1 -25     render/katerenderrange.h  
 D             smart (directory)  
 M  +0 -2      tests/completion_test.cpp  
 M  +0 -81     view/kateview.cpp  
 M  +0 -17     view/kateview.h  
 M  +0 -1      view/kateviewhelpers.cpp  
 M  +7 -184    view/kateviewinternal.cpp  
 M  +1 -20     view/kateviewinternal.h  
</pre>

 [1]: /2010/07/13/kde-4-5-smartrange-movingrange/
 [2]: /2010/07/23/movingranges-moving-on/