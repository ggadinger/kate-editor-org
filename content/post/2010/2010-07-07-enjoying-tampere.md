---
title: Enjoying Tampere
author: Dominik Haumann

date: 2010-07-06T22:11:00+00:00
url: /2010/07/07/enjoying-tampere/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2010/07/enjoying-tampere.html
categories:
  - Events

---
As always, the KDE conference has its funny sides, as you can see on the photo: 4 Kate developers shaping the future of the (as you all know) most awesome application: Kate :-)  
<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://2.bp.blogspot.com/_JcjnuQSFzjw/TDOqNVjjtwI/AAAAAAAAAEE/iwPLElohTnE/s1600/prettywoman-small.jpeg"><img style="margin: 0px auto 10px; display: block; text-align: center; cursor: pointer; width: 400px; height: 300px;" src="http://2.bp.blogspot.com/_JcjnuQSFzjw/TDOqNVjjtwI/AAAAAAAAAEE/iwPLElohTnE/s400/prettywoman-small.jpeg" alt="" id="BLOGGER_PHOTO_ID_5490919516796073730" border="0" /></a>