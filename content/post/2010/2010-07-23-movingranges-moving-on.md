---
title: MovingRanges moving on ;)
author: Christoph Cullmann

date: 2010-07-23T07:55:09+00:00
url: /2010/07/23/movingranges-moving-on/
categories:
  - Developers
tags:
  - planet

---
As with any new code, during adoption bottlenecks show up.

For example the rendering of text lines with many ranges inside was quiet slow. This is now partly addressed by Milian Wolff, thanks a lot.

An other bottleneck was the assumption, that it is fast enough to hash the ranges just by their block and iterate over all of them to search the ranges matching a specific line. This does scale well enough for KatePart itself, but KDevelop creates multi-thousand ranges for small documents. To improve this, an internal special mapping was implemented by David Nolden for ranges which don&#8217;t span more than one line. For them an efficient line => range mapping is easy and not to costly.

If both changes are tested a bit more, they can be backported in time for KDE 4.5, which will allow a good usability of KatePart for KDevelop once again after the rewrite, given other bugs are fixed.