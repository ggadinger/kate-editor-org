---
title: File Tree plugin and Kate in KDE SC 4.6
author: Thomas Fjellstrom

date: 2010-10-20T07:38:29+00:00
url: /2010/10/20/file-tree-plugin-and-kate-in-kde-4-6/
categories:
  - Users
tags:
  - planet

---
Hi! It&#8217;s me again. Got some exciting news.

The file tree is now the default (and only) document list/view widget. It actually took me by surprise. After I got the list mode and other features implemented, the old document list was unceremoniously ripped out, and extensions were added to allow kate to always load a given plugin, and give its settings tab a little more prominence.

Since then however I&#8217;ve fixed a few issues regarding the settings in the plugin. The file tree settings tab now sets the plugins global settings, and will clear out the setting overrides for the current session so they will get pulled from the global settings. And I just finished up adding back the &#8220;Go&#8221; menu, and the &#8220;next&#8221; and &#8220;prev&#8221; document shortcuts.

Just to be clear, you are NOT forced to use a tree view from now on. That is just the current default. You can switch to list mode using either the settings dialog (which will apply to kate globally, minus any sessions you have used the document view context menu to change the mode for), or using the context menu on the document view (file tree) widget.

Thank you to everyone who has used my plugin in the past, and to everyone who will be using it in the future :)