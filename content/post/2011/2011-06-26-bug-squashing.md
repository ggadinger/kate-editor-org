---
title: Bug Squashing
author: Dominik Haumann

date: 2011-06-25T22:41:22+00:00
url: /2011/06/26/bug-squashing/
categories:
  - Developers
  - Users
tags:
  - planet

---
We&#8217;ve managed to push our bugs down from ~430 to ~310 during the last two days. Some bugs are not valid anymore, but lots of bugs also <a title="Kate Git Activity" href="https://projects.kde.org/projects/kde/kdebase/kate/activity" target="_blank">really have been fixed</a>. So in KDE 4.7 we will have the [best Kate release][1] ever :-)

![Going to Desktop Summit 2011][2]

 [1]: / "Kate Homepage"
 [2]: /wp-content/uploads/2011/06/DS2011banner.png