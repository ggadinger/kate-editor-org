---
title: Python plugin user guide
author: shaheed

date: 2012-06-26T21:12:44+00:00
url: /2012/06/26/python-plugin-user-guide/
pw_single_layout:
  - "1"
categories:
  - Common
tags:
  - python

---
KDE 4.9 has branched, and so the [Pâté plugin][1] &#8220;host&#8221; for Python plugins has moved from Kate&#8217;s playground to be a mainstream plugin. Since a plugin-for-plugins might be a bit confusing, here is a quick tour of how it is managed and works, and especially a few rough edges it still has.

**NOTE** If you&#8217;ve installed this from a package, hopefully your distro will have added kate.so under PyKDE4 as needed, but if you are building from source, see the notes at the end of this post.

Start by enabling the enabling the Pâté plugin:

[<img class="alignnone size-medium wp-image-1878" src="/wp-content/uploads/2012/06/snapshot21-300x166.png" alt="" width="300" height="166" srcset="/wp-content/uploads/2012/06/snapshot21-300x166.png 300w, /wp-content/uploads/2012/06/snapshot21.png 694w" sizes="(max-width: 300px) 100vw, 300px" />][2]

That should cause the Pâté plugin&#8217;s configuration page to pop into existence, but you may need to press the Reload button to see the right hand pane filled in like this:

[<img class="alignnone size-medium wp-image-1848" src="/wp-content/uploads/2012/06/snapshot12-300x166.png" alt="" width="300" height="166" srcset="/wp-content/uploads/2012/06/snapshot12-300x166.png 300w, /wp-content/uploads/2012/06/snapshot12.png 694w" sizes="(max-width: 300px) 100vw, 300px" />][3]

Enable specific Python plugins of interest, and press the Reload button. The comment will be set to &#8220;Loaded&#8221;, then close and reopen the dialog (rough edge #1) to see any new configuration pages that the plugin provides:

[<img class="alignnone size-medium wp-image-1849" src="/wp-content/uploads/2012/06/snapshot13-300x232.png" alt="" width="300" height="232" srcset="/wp-content/uploads/2012/06/snapshot13-300x232.png 300w, /wp-content/uploads/2012/06/snapshot13.png 713w" sizes="(max-width: 300px) 100vw, 300px" />][4]

Go back to the Pâté plugin&#8217;s configuration page, and now look at the second tab:

[<img class="alignnone size-medium wp-image-1850" src="/wp-content/uploads/2012/06/snapshot14-300x232.png" alt="" width="300" height="232" srcset="/wp-content/uploads/2012/06/snapshot14-300x232.png 300w, /wp-content/uploads/2012/06/snapshot14.png 713w" sizes="(max-width: 300px) 100vw, 300px" />][5]

Use the combo box to select one of the built-in support modules (kate and kate.gui) or any of the loaded plugins. The first tab shows HTML documentation for the Python code and you can see any actions it has added into the Kate UI on the second tab (this will be empty for the built-in modules, but not for console):

[<img class="alignnone size-medium wp-image-1851" src="/wp-content/uploads/2012/06/snapshot15-300x232.png" alt="" width="300" height="232" srcset="/wp-content/uploads/2012/06/snapshot15-300x232.png 300w, /wp-content/uploads/2012/06/snapshot15.png 713w" sizes="(max-width: 300px) 100vw, 300px" />][6]

It looks as though the Python console has added one entry to the View menu, alog with a shortcut. And so it has:

[<img class="alignnone size-medium wp-image-1852" src="/wp-content/uploads/2012/06/snapshot16-172x300.png" alt="" width="172" height="300" srcset="/wp-content/uploads/2012/06/snapshot16-172x300.png 172w, /wp-content/uploads/2012/06/snapshot16.png 298w" sizes="(max-width: 172px) 100vw, 172px" />][7]

Yaay! See if you can guess what to do next :-)

&nbsp;

**IF YOU HAVE BUILT FROM SOURCE**

And then installed the results in say /usr/local, attempting to enable the plugin may result in a crash with something this on the terminal:

<pre style="padding-left: 30px">&gt;  File "/home/.../kate/plugins/pate/kate/__init__.py", line 38, in &lt;module&gt; from PyKDE4.kate import Kate
&gt; No module named kate
&gt; Could not import kate"</pre>

This is because the Pate build should install kate.so as part of the PyKDE4 bindings, but typically, a developer build will install this locally, and not to where the remaining system-supplied PyKDE4 files are installed:

<pre style="padding-left: 30px">Install the project...
 -- Install configuration: "RelWithDebInfo"
 -- Up-to-date: /usr/local/lib/python2.7/dist-packages/PyKDE4/kate.so
 ...</pre>

The system-supplied PyKDE4 binding modules should be in Python&#8217;s sys.path:

<pre style="padding-left: 30px">srhaque&gt; python
 Python 2.7.3 (default, Apr 20 2012, 22:39:59)
 [GCC 4.6.3] on linux2
 Type "help", "copyright", "credits" or "license" for more information.
 &gt;&gt;&gt; import sys
 &gt;&gt;&gt; print sys.path
 ['', '/usr/lib/python2.7', '/usr/lib/python2.7/plat-linux2', '/usr/lib/python2.7/lib-tk', '/usr/lib/python2.7/lib-old', '/usr/lib/python2.7/lib-dynload', '/usr/local/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages', '/usr/lib/python2.7/dist-packages/PIL', '/usr/lib/python2.7/dist-packages/gst-0.10', '/usr/lib/python2.7/dist-packages/gtk-2.0', '/usr/lib/pymodules/python2.7']</pre>

For me, Ubuntu puts PyKDE4 at /usr/lib/python2.7/dist-packages/PyKDE4, so we need a link from there to the actual location:

<pre style="padding-left: 30px">srhaque&gt; ls -l /usr/lib/python2.7/dist-packages/PyKDE4/kate.so</pre>

<pre style="padding-left: 30px">lrwxrwxrwx 1 root root 53 May 20 09:51 /usr/lib/python2.7/dist-packages/PyKDE4/kate.so -&gt; /usr/local/lib/python2.7/dist-packages/PyKDE4/kate.so</pre>

Note that just having this location in PYTHONPATH and/or in sys.path does not work because the PyKDE4 needs an \_\_init\_\_.py to be seen (see the one in the system directory to understand why).

I&#8217;ve not thought of a good way to simplify this for developers, and I assume end-users won&#8217;t see it because we&#8217;ll install in the default path. Suggestions/fixes welcome.

&nbsp;

 [1]: /2012/06/26/extending-kate-with-pytho/ "Pâté plugin"
 [2]: /wp-content/uploads/2012/06/snapshot21.png
 [3]: /wp-content/uploads/2012/06/snapshot12.png
 [4]: /wp-content/uploads/2012/06/snapshot13.png
 [5]: /wp-content/uploads/2012/06/snapshot14.png
 [6]: /wp-content/uploads/2012/06/snapshot15.png
 [7]: /wp-content/uploads/2012/06/snapshot16.png