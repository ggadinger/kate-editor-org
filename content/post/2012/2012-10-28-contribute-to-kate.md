---
title: Contribute to Kate
author: Dominik Haumann

date: 2012-10-28T18:45:02+00:00
url: /2012/10/28/contribute-to-kate/
pw_single_layout:
  - "1"
categories:
  - Developers
tags:
  - planet

---
Currently we are at the Kate/KDevelop sprint in Vienna. Christoph and me took the time to look a bit around in the net for Kate, and more or less accidently stumbled over quite a lot of extensions by our users. Here are some examples:

  * <a title="Jump to next/previous paragraph" href="http://kucrut.org/move-cursor-to-next-prev-paragraph-in-kate/" target="_blank">script to jump to next/previous paragraph</a>
  * several really <a title="Kate Highlighting Files on kde-files.org" href="http://kde-files.org/index.php?xcontentmode=680&PHPSESSID=855b2f88554fd7e88f0eba142655e628" target="_blank">good highlighting files</a>, e.g. for <a title="C++11 Highlighting for Kate" href="http://kde-files.org/content/show.php/?content=90660" target="_blank">C++11</a>
  * several <a title="Kate Python plugins" href="https://github.com/zaufi/kate-pate-plugins" target="_blank">Kate plugins written in Python</a>
  * many more python plugins for Kate, even officially <a title="More Kate Python Plugins" href="http://pypi.python.org/pypi/Kate-plugins" target="_blank">listed in the Python index</a>
  * there is more, I won&#8217;t list it all :)

Kate has just about 20 plugins written in C++. And, ironically, there are more plugins for Kate written in python. This is totally awesome, but still, the Kate developers didn&#8217;t even know about it ?¿? :-o

So we started contacting these potential contributors, and also asked why they e.g. upload it in some other git repository. One time, we got the answer, that they uploaded it somewhere to share it with other users. But really, the perfect way to share additions for Kate is to put it into the Kate git repository directly. This is how open source works&#8230;

So if you make additions or changes to Kate, [please let us know][1], <a title="KDE Reviewboard" href="http://techbase.kde.org/Development/Review_Board" target="_blank">create a patch and send it to review board</a>, come to our <a title="KDE Developer Sprints" href="https://sprints.kde.org/" target="_blank">developer sprints</a>, come to the <a title="Akademy" href="http://akademy.kde.org/" target="_blank">annual KDE conferences</a>&#8230; Talk to us, usually we are all pretty friendly and really happy about every single contribution!

 [1]: /join-us/ "Join Us"