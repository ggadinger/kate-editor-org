---
title: KDE Code Formatting
author: Christoph Cullmann
date: 2021-03-06T15:20:00+02:00
url: /post/2021/2021-03-06-kde-code-formatting/
---

## Short history of the 'KDELIBS' coding style

Once upon a time, in the monolithic KDELIBS world, we had some document describing the KDELIBS coding style.

Over the years, that evolved a bit and ended up [here as Frameworks Coding Style](https://community.kde.org/Policies/Frameworks_Coding_Style).

As noted there, it is more or less the same stuff Qt does style wise.

## How was that coding style handled in practice?

Actually, this styling was really never enforced on a global scale.

During the "we split KDELIBS up into Frameworks" time, on the initial import, the code was once run through astyle to ensure that coding style was kept.

But after the initial import, nothing of that sort happened anymore (at least not in some coordinated fashion).

Beside, for non-Frameworks, such a mandatory style application never happened.
Actually, it was never be agreed that this style is mandatory beside for KDELIBS itself, anyways.

Naturally, individual sub-projects/maintainers started to enforce either the stuff linked above or individual similar styles through different means.

e.g. in [kate.git](https://invent.kde.org/utilities/kate) we noted in the README that we wanted to follow that style.
That was it ;=)

## Why is that sub-optimal?

Over the years, a lot of code started to diverge from the wanted style, both in Frameworks and other parts that actually want to use the above coding style.

On the other side, as there was no easy way to apply the coding style on changes (beside if you start to tell each new-comer how to use astyle or other scripts), often new contributors struggled with the purely style related comments they got on their merge requests.
I can understand that, you step up to fix some bug and the first feedback you get: please some spaces here, some spaces less here and the '{' into the next line.

## The solution: include the formatting into the workflow

After years of no improvements in that area, we now are at a state that allows a much better integration of the coding style handling in our workflow.

### Central managed clang-format configuration

Our [Extra CMake Modules](https://invent.kde.org/frameworks/extra-cmake-modules) Framework now provides a centrally maintained clang-format configuration.

As you can see, after the introduction over one year ago, this got more [fine tuned](https://invent.kde.org/frameworks/extra-cmake-modules/-/commits/master/kde-modules/clang-format.cmake) to match the style we actually use in Frameworks.

### CMake functionality to re-format your stuff

The Extra CMake Modules provides CMake functions to apply the style to your project, too.

Let's take a look how that is e.g. used in [KTextEditor](https://invent.kde.org/frameworks/ktexteditor):

{{< highlight cmake >}}
include(KDEClangFormat)

file(GLOB_RECURSE ALL_CLANG_FORMAT_SOURCE_FILES *.cpp *.h)
kde_clang_format(${ALL_CLANG_FORMAT_SOURCE_FILES})
{{< /highlight >}}

This will allow you to later just do e.g.

{{< highlight bash >}}
make clang-format
{{< /highlight >}}

to reformat all your stuff with the proper ECM provided clang-format configuration.

### Provide Git hooks

Still, you might easily forget to run this and still commit stuff that doesn't match.
Or, for new contributors, to create merge requests containing such potential badly formatted stuff.

To advert that, ECM provides functionality to auto-register Git hooks, too, that will avoid that you commit such stuff.

{{< highlight cmake >}}
include(KDEGitCommitHooks)

kde_configure_git_pre_commit_hook(CHECKS CLANG_FORMAT)
{{< /highlight >}}

Now, if you forget to adhere to the stuff that is verifiable by clang-format, you will get a message like

{{< highlight bash >}}
ERROR: You have unformatted changes, please format your files. You can do this using the following commands:
       git clang-format --force # format the changed parts
       git clang-format --diff # preview the changes done by the formatter
{{< /highlight >}}

on a commit try.

This should make it much easier to avoid such style mistakes to slip in.

## What's missing?

Naturally, all this is cool, but not that useful, if not really applied.

Ahmad and Alexander are currently applying this to more or less all Frameworks, the progress can be tracked in [this issue](https://invent.kde.org/frameworks/kcoreaddons/-/issues/1).

Alexander and others do the same for Plasma, as can be tracked in [yet another issue](https://invent.kde.org/plasma/plasma-workspace/-/issues/9).

Naturally, if you want to help out, that is always welcome, just show up in the above issues to have this coordinated ;=)
