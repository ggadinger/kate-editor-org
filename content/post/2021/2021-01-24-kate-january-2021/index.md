---
title: The Kate Text Editor - January 2021
author: Christoph Cullmann
date: 2021-01-24T14:40:00+02:00
url: /post/2021/2021-01-24-kate-january-2021/
---

A lot of stuff happened for Kate and it's foundation KTextEditor & KSyntaxHighlighting in January 2021 ;)

I will just briefly summarize what is going one feature wise.

## Quick Open

Already two weeks ago I pointed out the improvements to our [Quick Open](/post/2021/2021-01-10-kate-quick-open/) dialog.

<p align="center">
    <a href="/post/2021/2021-01-10-kate-quick-open/images/kate-21-04-quick-open.png" target="_blank"><img width=700 src="/post/2021/2021-01-10-kate-quick-open/images/kate-21-04-quick-open.png"/></a>
</p>

It not only got some nice visual refresh but a much better fuzzy matching algorithm.

The fuzzy matching algorithm is on its way to be upstream to [KCoreAddons](https://invent.kde.org/frameworks/kcoreaddons/-/merge_requests/70) to be used by more parts of the KDE universe.

Praise to Waqar Ahmed for implementing this and pushing it to upstream.
And thanks to Forrest Smith for allowing us to use his matching algorithm under LGPLv2+!

## HUD style command palette

;=) I stole that naming from Nate's [This week in KDE: the Plasma 5.21 beta is here!](https://pointieststick.com/2021/01/23/this-week-in-kde-the-plasma-5-20-beta-is-here/) post.

<p align="center">
    <a href="/post/2021/2021-01-24-kate-january-2021/images/kate-hud.png" target="_blank"><img width=700 src="/post/2021/2021-01-24-kate-january-2021/images/kate-hud.png"/></a>
</p>

This dialog provides the same fuzzy matching we have in Quick Open but applies it to all registered actions of the application.

There is some menu items for it?
It will find it.

Besides, it even is able to trigger sub menus.

As it works on the KActionCollection of the application without any other dependencies, this might be upstreamed later to e.g. KXMLGui to be usable for other KDE applications, too.

Thanks for this to Waqar Ahmed, too!

## Search & Replace

The *Search and Replace* tool view provided by the corresponding plugin is a central feature of Kate.

I use it a lot myself, especially in combination with the projects plugin.
Just type *pgrep Kate* in your Kate command line (F7) and you get all matches of that in your current active project.
Replaced my usual *grep -r* or *git grep* inside the Kate terminal tool view years ago.

Both representation and architectural wise this now got some overhaul (and more to come).

<p align="center">
    <a href="/post/2021/2021-01-24-kate-january-2021/images/kate-search-and-replace.png" target="_blank"><img width=700 src="/post/2021/2021-01-24-kate-january-2021/images/kate-search-and-replace.png"/></a>
</p>

We now use the same theme as inside the KTextEditor editor views.
This means the matches will look exactly like the text in your editor, same font, same colors.

In addition, the plugin uses now a proper model-view separation and we will try to get multi-threaded search working, too.
At the moment still one background thread does the disk search, this [will be improved](https://invent.kde.org/utilities/kate/-/issues/21).

Thanks to Kåre Särs & Waqar Ahmed, for working on this.

## Semantic Highlighting via LSP

Another nifty feature is the improved semantic highlighting support inside the LSP plugin.
This now more properly tries to use the same theme colors as KTextEditor & KSyntaxHighlighting provide, too.

There is still room for improvements, like allowing themes to [specify more colors for this](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/142).
But at least as a start, it already works ok.

See below the a file inside the Kate project without semantic highlighting enabled:

<p align="center">
    <a href="/post/2021/2021-01-24-kate-january-2021/images/kate-without-semantic-highlighting.png" target="_blank"><img width=700 src="/post/2021/2021-01-24-kate-january-2021/images/kate-without-semantic-highlighting.png"/></a>
</p>

And here with that feature on:

<p align="center">
    <a href="/post/2021/2021-01-24-kate-january-2021/images/kate-with-semantic-highlighting.png" target="_blank"><img width=700 src="/post/2021/2021-01-24-kate-january-2021/images/kate-with-semantic-highlighting.png"/></a>
</p>

:=) Waqar worked on this, too, the LSP changes were reviewed by Mark Nauwelaerts.

## Summary & Outlook

This is not even all that happened in 2021 up to now ;)

As you can see on [our team page](/the-team/) a lot of new people helped out in the scope of the last year.
I hope to see more people showing up there as new contributors.
It is a pleasure that Waqar Ahmed & Jan Paul Batrina now have full KDE developer accounts!

Especially Waqar came up with a lot of [nifty ideas](https://invent.kde.org/utilities/kate/-/issues/20) what could be fixed/improved/added and he did already do a lot of work to actually get these things done!

I actually wanted to write earlier about what cool new stuff is there, but had too much review requests to look after.
Great!
;=) No I can read review request instead of light novels in the evening.

In my [Kate is 20 years old! post from December 14, 2020](/post/2020/2020-12-14-kate-is-20-years-old/) I added summaries of the merge requests we got since we are on our GitLab [invent.kde.org](https://invent.kde.org) instance:

* 115 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
* 41 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
* 113 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

Today, a bit over one month later, these numbers look like:

* 170 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
* 82 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
* 150 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

I would say that is a nice increase ;=)

Naturally, there are small and large merges, but still, KTextEditor's requests doubled in a bit over one month.

Let's hope this trend continues!

Thanks to all people that contribute, let's rock!

## Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/l3zna4/the_kate_text_editor_january_2021_status_update/).
