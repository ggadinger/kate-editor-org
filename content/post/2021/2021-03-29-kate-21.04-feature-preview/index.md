---
title: Kate 21.04 Feature Preview
author: Christoph Cullmann
date: 2021-03-29T21:54:00+02:00
url: /post/2021/2021-03-29-kate-21.04-feature-preview/
---

## Soon KDE Gear 21.04 will be out, including Kate

Soon [KDE Gear 21.04](https://community.kde.org/Schedules/KDE_Gear_21.04_Schedule) will be released.
This will include the Kate text editor.

### KDE what?

If you wonder: what is KDE Gear?
That is the new name for the [KDE release service](https://jriddell.org/2021/03/22/kde-gear-21-04-apps-send-us-your-features/).

This had different names in the past, like "KDE Applications" that were misleading, as not all KDE based applications are released together like this.
For example neither [Krita](https://www.krita.org) is part of KDE Gear nor is [KDevelop](https://www.kdevelop.org), to just name a few.

After some releases with the very neutral "release service" moniker, we are now back to have some more recognizable branding for this bundle of applications: KDE Gear.
I like that ;=)

### Why is Kate inside that bunch of applications?

What is the benefit of this service for application maintainers/developers?

One doesn't need to take care of the release schedule, branching, tagging, beta/.../release tar ball creation.
This includes proper [release announcements](https://kde.org/announcements/releases/2020-12-apps-update/), too.
That is handled by the release team.
Just look at [the long list of steps](https://community.kde.org/Schedules/KDE_Gear_21.04_Schedule) needed for 21.04.
Naturally the release team is for sure open to more people joining to share the work!

A proper string freeze in ensured to give our great team of translators/documentation writers/... time to do their work properly.
A big thanks to all people working on this!
Look at the [awesome statistics](https://l10n.kde.org/stats/gui/stable-kf5/team/) of the state of translations for the current stable release.
Kate alone would never attract that many people to take care of this large set of languages.

I am very happy that Kate can be part of such a great release service.
Albert, Jonathan and all others working on this: a big thanks from the Kate team!

I for sure would not have found time to make regular Kate releases on my own!
All these people ensure you enjoy a stable Kate release every few months.
I must confess, Kate was always part of such a service and I actually never, ever, made any own release in 20 years.
Call me a lazy guy.
The only thing we ever did on our own was to promote new Windows or macOS builds.

### Big thanks to the KDE community!

After some random name calling above, to make it clear: Kate without the awesome KDE community would neither have soon a release nor still exist.

Let’s take a look at the Akademy 2020 group photo below (CC BY 4.0, created by the Akademy team) to have a small idea of this community:

![Akademy 2020 group photo](/post/2021/2021-03-29-kate-21.04-feature-preview/images/akademy2020_group_photo_1080.png)

Thanks to all, both for helping on the technical level and to keeping (at least me) interested in KDE related stuff.

### Isn't that very inflexible?

You might now argue that this service will make it impossible to e.g. release versions in-between.

I must argue, the current cadence of 4 months between the releases is already short and I doubt that Kate can have faster releases with the current manpower and not dropping e.g. the localization quality.

On the other side, the KDE Binary Factory provides nightly builds of the current development branch of many KDE projects.
Just head over to [our download page](/get-it/) to get links to these builds for Kate.
For sure, they have not the quality of the releases, but we normally keep the development branch good enough for daily work, at least I use local builds of that daily.

Naturally, if you want to be even more on the bleeding edge, [build the stuff locally](/build-it/).
That is a good way to [start contributing](/join-us/), too!

## What's new in the upcoming 21.04 release?

Ok ;=)
Now, after introducing KDE Gear 21.04 and some endless praise of others, let's take a look what Kate 21.04 will provide.

Don't try to judge the individual features by the sorting order below, for sure every user will have here different favorites ;)

### Quick Open with Fuzzy Matching

Our quick open implementation got redone.
We now have both a highly improved representation of the quick open results and a very nice fuzzy matching used for the filtering.

Just compare the old 20.12 variant:

![Quick Open in 20.12](/post/2021/2021-01-10-kate-quick-open/images/kate-20-12-quick-open.png)

with the new 21.04 bliss:

![Quick Open with Fuzzy Matching in 21.04](/post/2021/2021-01-10-kate-quick-open/images/kate-21-04-quick-open.png)

For more details of this, please refer to [this blog post](/post/2021/2021-01-10-kate-quick-open/).

I think the new version very well compares with what other modern editors like Atom or VS Code provide.

### HUD style command palette

A completely new feature of 21.04: a quick open like dialog that let's you activate any action the Kate application provides.

![HUD style command palette with Fuzzy Matching in 21.04](/post/2021/2021-01-24-kate-january-2021/images/kate-hud.png)

Naturally this features the same great fuzzy matching as quick open itself and the same UI.
You can even trigger menu and their sub-actions without any issue.

For more details, refer to our [January summary blog](/post/2021/2021-01-24-kate-january-2021/).

### Improved Search In Files

Our search in files plugin got a major overhaul.

We have now a better results representation in the proper editor font and colors:

![Search in files in 21.04](/post/2021/2021-01-24-kate-january-2021/images/kate-search-and-replace.png)

Beside this, the search engine got parallelized to try to utilize all CPU cores you have in your machines.
The below video will show the astonishing new performance.

<video width=100% controls>
<source src="/post/2021/2021-01-29-search-in-files-multi-threading//videos/search-in-action.webm" type="video/webm">
Your browser does not support the video tag :P</video>

We no longer need to hide behind Atom or VS Code here!
For more details, refer to [this blog post](/post/2021/2021-01-29-search-in-files-multi-threading/).

### Improved LSP (Language Server Protocol) support

Over the last months, multiple small improvements were done to our LSP support.

For people not really familiar with that term, in short: the LSP plugin provides integration with e.g. clangd for C++ to provide on-the-fly code checking for warnings/errors, go to definition/declaration capabilities, symbol outline, etc.

If LSP works for your language, and we out of the box support a lot of LSP servers that at least cover stuff like C/C++, Go, Rust and more, Kate will be like a light weight IDE.

A nice thing is the new overhauled popup to show contextual information, no longer a hard to read normal tooltip:

![LSP tooltip](/post/2021/2021-02-14-kate-valentines-day/images/kate-lsp-tooltip.png)

Another very visible new features is support for semantic highlighting:

![LSP semantic highlighting in 21.04](/post/2021/2021-01-24-kate-january-2021/images/kate-with-semantic-highlighting.png)

Whereas this is not on the level of what [KDevelop](https://www.kdevelop.org) provides, this implementation has the benefit to be  maintained and improved inside the LSP server like clangd.
This means it should without any effort on our side keep track of new C++ standards.
Given our team is small, this is a really great benefit of the LSP concept.

### Integrated Git support

Kate has since long the project plugin that allows to seamlessly load Git based projects without any extra configuration.
Just open some file inside a Git repository, you will get a properly filled project tree view and some local terminal.
Beside you can search in all project files with the search plugin and quick open will allow fast navigation.

But there was no built in way to actually work with the Git repository, beside the integrated terminal views.
With 21.04 this will be a thing of the past.
Given that on Windows we have at the moment no supported terminal integrated, there this is even the first time to be able to do Git stuff inside Kate!

#### Checkout or create some branch

![Screenshot of Kate Git branch selector](/post/2021/2021-02-14-kate-valentines-day/images/kate-git-checkout.png)

#### Stash stuff

![Screenshot of Kate Git stash context menu](/post/2021/2021-02-28-kate-february-2021/images/kate-git-stash.png)

#### Stage your files for commit or diff

![Screenshot of Kate Git diff/staging context menu](/post/2021/2021-02-28-kate-february-2021/images/kate-git-stage.png)

#### Do the commit and push afterwards

![Screenshot of Kate Git commit dialog](/post/2021/2021-02-28-kate-february-2021/images/kate-git-commit.png)

Naturally, this is the first release of all these things.
There will be bugs, please report them or even better: help to iron them out!
The above list is not exhaustive, try to explore the new Git tool view a bit to find out what is possible ;=)

### Generic output tool view

In 20.12, if the LSP stuff had issues, it did report that inside some tab inside the LSP tool view.
For 21.04, we had now the same situation for the Git integration: shall we just add an extra tab, there, too?

We went for the more generic solution: Some central tool view that can be utilized by Kate itself or any of the plugins.
You can configure which level of message will actually raise this tool view to avoid constant interruption by trivial info messages.

![Screenshot of Kate output view](/post/2021/2021-02-28-kate-february-2021/images/kate-output.png)

The current variant is for sure not the last iteration of this and has not that many features beside filtering and a copy action.

## That's it ;=)

For sure I did not list all things that happened for 21.04.
I did not even list all stuff I mentioned in the last months in my blog posts here, but just the stuff I found most interesting.

I hope this made you interested enough to give Kate 21.04 a try as soon as it is out.
But even more important for us: Perhaps you have even interest [to join our team](/join-us/) and make Kate more awesome.

There are a lot of editors out there, both old ones like Emacs or Vim and new ones like Atom and VS Code.

Kate is a small project, we have few contributors and for sure not as many features as the mentioned editors above.
But still, I use Kate daily, so do others.
I think it is a good tool if it fits your working pattern.
And I hope the new release will broaden the working patterns we can properly support.

## Comments?

A matching thread for this can be found here on [r/KDE](https://www.reddit.com/r/kde/comments/mfy15q/kate_2104_feature_preview/) and [r/Linux](https://www.reddit.com/r/linux/comments/mfy0w7/the_kate_text_editor_2104_feature_preview/).

<p align="center">
    <img width=256 src="/images/kate.svg">
</p>
