---
title: Kate Developer Sprint 2008
author: Christoph Cullmann

date: 2008-03-25T12:53:43+00:00
url: /2008/03/25/kate-developer-sprint-2008/
categories:
  - Events

---
In April there will be a Kate Developer Sprint, similar to previous sprints for [Decibel][1], [Akonadi][2], [KDevelop][3] and others. This is a great opportunity as developers interested in development of KDE&#8217;s text editor will discuss what to do to make Kate [the best text editor][4] on earth. This also means lots of polishing so that Kate in KDE 4.1 will shine even more! The meeting will take place from 2008-04-11 till 2008-04-13. The location is hosted by [basysKom][5] in Darmstadt, Germany. Many thanks goes especially to the [KDE e.V.][6] for their financial support. If you are interested in Kate development, join our [mailing list][7]. More to come, stay tuned! 

The event is organized by Dominik Haumann, and is supported by the kde.ev.

On the agenda is discussing Kates future development and improving scripting capabilities amongst others.

 [1]: http://dot.kde.org/1174669960/
 [2]: http://dot.kde.org/1203626978/
 [3]: http://dot.kde.org/1202500128/
 [4]: http://www.linuxquestions.org/questions/2007-linuxquestions.org-members-choice-awards-79/text-editor-of-the-year-610219/
 [5]: http://www.basyskom.de
 [6]: http://ev.kde.org
 [7]: https://mail.kde.org/mailman/listinfo/kwrite-devel