---
title: 'Akademy 08: Kate Flashback'
author: Dominik Haumann

date: 2008-08-14T21:28:00+00:00
url: /2008/08/14/akademy-08-kate-flashback/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2008/08/akademy-08-kate-flashback.html
categories:
  - Events

---
This Akademy&#8217;s Kate changes include

  * fix: drag & drop of text
  * code completion: only show group header if the group name is not empty
  * reintroduction of buffer blocks in Kate&#8217;s document buffer (one buffer contains up to 4096 lines). The blocks build a linked list. Editing a 500 MB file kind of works now again. It&#8217;s still rather slow, though.
  * more speedup in Kate&#8217;s document buffer
  * Kate is using KEncodingProber instead of KEncodingDetector now
  * generate internal version of KatePart automatically, so developers don&#8217;t have to adapt it manually each release
  * python encoding detection plugin that warns if encoding is not correct while saving
  * new plugin: Backtrace browser, mainly for developers
  * find in files: several speed optimizations
  * find in files: progress indicator while search is active
  * find in files: redesign of workflow. Search happens with non-modal dialog and results are shown in toolviews.
  * lots of vi mode changes
  * lots of bugs closed, mainly old ones
  * some real bug fixes&#8230;
  * things I forgot