---
title: 'Kate Meeting: Day 1 and 2'
author: Dominik Haumann

date: 2008-04-15T13:29:00+00:00
url: /2008/04/15/kate-meeting-day-1-and-2/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2008/04/kate-meeting-day-1-and-2.html
categories:
  - Events

---
[The Kate Developer Meeting][1] was a productive weekend and once more shows how important the developer sprints are. The summary will be on the dot shortly. Work already started on several topics. As everyone want screenshots, here we go: The new annotation interface available now in KTextEditor can be use to e.g. show svn annotations directly in kate:  
<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://3.bp.blogspot.com/_JcjnuQSFzjw/SAS_Qe1HbjI/AAAAAAAAAA0/W6C00c79l6k/s1600-h/kate-annotations.png"><img style="margin: 0px auto 10px; display: block; text-align: center; cursor: pointer; width: 333px; height: 271px;" src="http://3.bp.blogspot.com/_JcjnuQSFzjw/SAS_Qe1HbjI/AAAAAAAAAA0/W6C00c79l6k/s400/kate-annotations.png" alt="" id="BLOGGER_PHOTO_ID_5189482960512314930" border="0" /></a>basysKom&#8217;s coffee maching is simply the best: It can do everything, you just have to press the right key combos:<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://2.bp.blogspot.com/_JcjnuQSFzjw/SATADO1HbkI/AAAAAAAAAA8/1_eFQ6eSaew/s1600-h/P1020906.JPG"><img style="margin: 0px auto 10px; display: block; text-align: center; cursor: pointer;" src="http://2.bp.blogspot.com/_JcjnuQSFzjw/SATADO1HbkI/AAAAAAAAAA8/1_eFQ6eSaew/s400/P1020906.JPG" alt="" id="BLOGGER_PHOTO_ID_5189483832390676034" border="0" /></a>

 [1]: /news/development-sprint-results