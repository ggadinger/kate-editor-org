---
title: Kate in the Windows Store
author: Christoph Cullmann
date: 2019-09-12T18:25:00+02:00
url: /post/2019/2019-09-12-kate-in-the-windows-store/
---

Our Windows Store submission succeeded, we are now officially [in the store](https://www.microsoft.com/store/apps/9NWMW7BB59HW).

<p align="center">
    <a href="/post/2019/2019-09-12-kate-in-the-windows-store/images/kate-on-windows.jpeg" target="_blank"><img width=500 src="/post/2019/2019-09-12-kate-in-the-windows-store/images/kate-on-windows.jpeg"></a>
</p>

Try out how the Kate text editor performs on Windows for your personal workflow.

<p align="center">
    <a href="/post/2019/2019-09-12-kate-in-the-windows-store/images/kate-on-windows-split-view.jpeg" target="_blank"><img width=500 src="/post/2019/2019-09-12-kate-in-the-windows-store/images/kate-on-windows-split-view.jpeg"></a>
</p>

If you see issues and want to help out, contributions are welcome on our [GitLab instance](https://invent.kde.org/utilities/kate).

Our Windows team is small, any help is very welcome!
Thanks again to all the people that made it possible to use Kate nicely on Windows.
