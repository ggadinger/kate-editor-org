---
title: Web-Server Transition
author: Christoph Cullmann

date: 2019-04-08T20:45:00+00:00
excerpt: |
  Several years the kate-editor.org &amp; cullmann.io pages got hosted on a Hetzner root server.
  To reduce costs and switch away from old hardware they got now moved to a OpenVZ based virtual server at Host Europe.
  
  On both servers CentOS 7.x is running,...
url: /posts/webserver-transition/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/webserver-transition/
syndication_item_hash:
  - cbec4cdb3dd8b2ea7ca070308cbac18d
  - c4ebdea3680ad43d865a66f1e82539f2
  - 79cf5233c51fce0fea404cbd15a80f24
  - 9dca55846032087aaf448cebf84cfa90
  - b04dd606a1035828264e9a0bd384632a
categories:
  - Common

---
Several years the [kate-editor.org][1] & [cullmann.io][2] pages got hosted on a [Hetzner][3] root server. To reduce costs and switch away from old hardware they got now moved to a [OpenVZ][4] based virtual server at [Host Europe][5].

On both servers [CentOS][6] 7.x is running, it did always provide a stable foundation of the web services.

As with any server move in the past, I always need to search how to best move the data/config from one server to the other. To document this for me and others, here the quick way to move the basic things needed for web services using just plain [Apache][7] & [MariaDB][8].

The following steps assume you have installed the same packages on both machines and the new machine is allowed to ssh as root to the old one. If you have non-system users, you should create them with the same ids as on the old server.

For the following shell commands, the old server address is $SERV and the MariaDB root password is $PASS on both machines. Best use the raw IP as address if you are in parallel updating your DNS entries to avoid confusion (and wrong syncs).

**Attention: Wrong syncing of stuff can have disastrous consequences! Check all commands again before executing them, don&rsquo;t trust random people like me without verification!**

  * sync your data, assuming it is in /home and /srv/(ftp/www)

> rsync &ndash;delete -av root@$SERV:/home/ /home  
> rsync &ndash;delete -av root@$SERV:/srv/ftp /srv  
> rsync &ndash;delete -av root@$SERV:/srv/www /srv  
> 

  * transfer your databases

> ssh root@$SERV &ldquo;mysqldump -u root -p$PASS &ndash;all-databases > /root/db.sql&rdquo;  
> scp root@$SERV:/root/db.sql /root/  
> mysql -u root -p$PASS < /root/db.sql  
> 

  * sync configs (you might need more, this is just apache & vsftp)

> rsync &ndash;delete -av root@$SERV:/etc/httpd /etc  
> rsync &ndash;delete -av root@$SERV:/etc/letsencrypt /etc  
> rsync &ndash;delete -av root@$SERV:/etc/vsftpd /etc  
> 

  * get crontabs over for later re-use, store them in the root home

> rsync &ndash;delete -av root@$SERV:/var/spool/cron /root

Now all things should be there and after some service restarts e.g. [WordPress][9] powered pages should be up-and-running again.

I hope this short how-to helps others and allows me to avoid searching stuff in the future once again from scratch.

 [1]: /
 [2]: https://cullmann.io/
 [3]: https://www.hetzner.de/
 [4]: https://openvz.org/
 [5]: https://www.hosteurope.de/
 [6]: https://centos.org/
 [7]: https://httpd.apache.org/
 [8]: https://mariadb.org/
 [9]: https://wordpress.org/