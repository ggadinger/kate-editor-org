---
title: 'KTextEditor on Frameworks 5: Timing is everything'
author: Dominik Haumann

date: 2014-01-11T22:14:36+00:00
url: /2014/01/11/ktexteditor-on-frameworks-5-timing-is-everything/
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
This is a follow-up to Aaron&#8217;s blog <a title="Frameworks 5: Timing is everything" href="http://aseigo.blogspot.com/2014/01/frameworks-5-timing-is-everything.html" target="_blank">Frameworks 5: Timing is everything</a>, put into the perspective of Kate and the KTextEditor interfaces. In short, Aaron suggests application developers the following:

> <span style="line-height: 1.5;">When Frameworks 5 has a firm release schedule, then it makes sense to start coordinating application releases targets with that and syncing up development cycles.</span>

I agree with this entirely, provided it&#8217;s really about applications. In the context of Kate, this is not the case, since the term &#8216;Kate&#8217; usually also refers to the &#8216;KTextEditor&#8217; interfaces along with its implementation &#8216;Kate Part&#8217;.

In essence, the KTextEditor interfaces together with Kate Part provide a text editor in a library that can be used by applications, or more to the point, **KTextEditor+Kate Part build a framework**.

In fact, about a week ago, the KTextEditor interfaces and Kate Part were split out of the kate.git repository. So from now on, kate.git contains the Kate and KWrite applications. The **ktexteditor.git** module contains the KTextEditor interfaces and Kate Part.

ktexteditor.git is is a tier3 framework and meanwhile officially part of the frameworks initiative: if you want so, KDE now provides **58 frameworks** instead of the <a title="Frameworks 5 Announcement" href="http://dot.kde.org/2014/01/07/frameworks-5-tech-preview" target="_blank">previously announced 57</a>!

### Why timing is everything

Now what about timing? Started on November 28th 2013, Christoph pushed the first compiling version of Kate into the frameworks branch. This was very good timing, since the split of kdelibs into all the frameworks was more or less just finished at that time. Or to put it the other way round: Christoph started porting Kate pretty much as early as the KF5 initialive allowed us &#8211; cool!

Around that time by coincidence, Michal started to work on Kate in the master branch and committed lots of cool features. However, since Kate was already rudimentary ported to Qt5 and KF5, the code base diverged more and more, so merging got more complicated. Therefore, Michal started to work in Kate&#8217;s frameworks branch. And within _only a week_ (!), Kate, KWrite, the KTextEditor interfaces and Kate Part build without a single warning, meaning that the code was completely free of Qt4 and kde4support &#8211; _wow!_ :-)

Again, a month before would have been too early &#8211; so this was perfect timing. The result was a pretty stable and mostly bug-free Kate in mid of December.

Well, not completely free of porting bugs. For instance, file saving did not work correctly, ending up in data corruption. It turned out that this was an issue in the framework karchive, which David Faure was able to reproduce and fix. This is good timing in the sense that without Kate&#8217;s usage of karchive, the bug would have probably ended up in the <a title="Frameworks 5 TP1" href="http://dot.kde.org/2014/01/07/frameworks-5-tech-preview" target="_blank">technical preview TP1</a>. But it didn&#8217;t &#8211; yay! :-)

Last but not least, the Kate and KDevelop developers planned already months ago to have a joint Kate/KDevelop sprint from 18th to 26th of January, 2014. Given the current state of Kate and KTextEditor, this again is perfect timing, since most of the KTextEditor cleanup are already done. So the Kate developers can focus on the needs of e.g. KDevelop, on fine-tuning and implementing interfaces, etc. So in about two weeks, when our developer sprint will have ended, Kate will most probably shine as it never did before. And this is the case already _now_, even before a firm release date of <a title="Plasma 2 Technology Preview" href="http://dot.kde.org/2013/12/20/plasma-2-technology-preview" target="_blank">Plasma 2</a> exists.

This is great news for Kate and all applications that use the KTextEditor interfaces, as Kate/KTextEditor already now reached a maturity that they never had before.

And this is so awesome! I cannot think of better timing :-) Thanks to all who made this happen!