---
title: Auto-loading Projects in the Projects Plugin (Kate 5)
author: Dominik Haumann

date: 2014-10-12T12:18:00+00:00
url: /2014/10/12/autoloading-projects-plugin-kate-5/
categories:
  - Developers
  - Users
tags:
  - planet

---
Since KDE SC 4.10, Kate ships with the [Projects plugin][1]. This plugin provides an automatically generated structured list of files belonging to a project. Currently, in Kate 5, the Projects plugin looks like this:

[<img class="aligncenter size-full wp-image-3417" src="/wp-content/uploads/2014/10/projectsplugin.png" alt="Projects Plugin in Kate 5" width="1017" height="637" srcset="/wp-content/uploads/2014/10/projectsplugin.png 1017w, /wp-content/uploads/2014/10/projectsplugin-300x187.png 300w" sizes="(max-width: 1017px) 100vw, 1017px" />][2]

What&#8217;s new in the Project plugin in Kate 5 since some weeks is an auto-loading feature. In 4.x times you needed to create a [.kateproject file][3] that was then read by the Projects plugin to populate the listview. This still works in Kate 5, of course. But if a .kateproject file does not exists, you can now still read the file list from the version control system. To this end, auto-loading for the respective version control system needs to be enabled in the settings (enabled by default):

[<img class="aligncenter size-full wp-image-3420" src="/wp-content/uploads/2014/10/projectsplugin-autoload.png" alt="Autoloading Kate Projects" width="657" height="508" srcset="/wp-content/uploads/2014/10/projectsplugin-autoload.png 657w, /wp-content/uploads/2014/10/projectsplugin-autoload-300x231.png 300w" sizes="(max-width: 657px) 100vw, 657px" />][4]We hope this is useful to you :-)

 [1]: /2012/11/02/using-the-projects-plugin-in-kate/ "Using the Projects Plugin"
 [2]: /wp-content/uploads/2014/10/projectsplugin.png
 [3]: /2012/11/02/using-the-projects-plugin-in-kate/ ".kateproject file"
 [4]: /wp-content/uploads/2014/10/projectsplugin-autoload.png