---
title: Kate Licensing
author: Christoph Cullmann

date: 2014-01-03T07:12:02+00:00
url: /2014/01/03/kate-licensing/
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
Hi,

a long time ago, the license of most parts of Kate/KWrite/KatePart/KTextEditor was LGPLv2+ (in the old time, where it was named KWritePart/Kant/&#8230;, the original KWrite had that license originally, at least all parts of the component of it).

Then, we changed that to be LGPLv2 (only).

It seems, that was a poor choice, as we now run in v2 vs. v3 vs. any later version problems.

Most parts of the code are easy to relicense, as the contributors either acknowledged the request to change the license to that to the Kate team (on kwrite-devel@kde.org) or added themselves to the relicensecheck.pl in kde-dev-scripts.git.

KTextEditor is now LGPLv2+ only, which is nice ;)

KatePart is only missing the re-licensing of some files.

Kate has more files to look at, but should be less a problem, as it has less people that did commits, compared to the part.

So, if you didn&#8217;t already get a mail from me about some &#8220;please allow on kwrite-devel@kde.org for license change to LGPLv2+&#8221; and you know you have contributed, even if it was only some patch, it would be really nice to get some short &#8220;I am ok with LGPLv2+&#8221; on kwrite-devel@kde.org.

That will make it much easier to sort out the remaining issues!

This really would help to have no licensing issues coming up in the future years and further incompatibilities. I really would like to strife for LGPLv2+ only code in KTextEditor/KatePart/Kate, at least in the core parts (e.g. without the plugins), which seems to be realistic in the short term to reach.

Thanks a lot and a happy new year.