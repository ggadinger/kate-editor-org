---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /scss/get-it.scss
title: Baixe o Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux e Unix

+ Instale o [Kate](https://apps.kde.org/en/kate) ou [KWrite](https://apps.kde.org/en/kwrite) de [sua distribuição](https://kde.org/distributions/)

{{< appstream_badges >}}

+ [Pacote Snap do Kate no Snapcraft](https://snapcraft.io/kate)

{{< store_badge type="snapstore" link="https://snapcraft.io/kate" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Pacote AppImage da versão lançada (64bit) do Kate](https://binary-factory.kde.org/job/Kate_Release_appimage-centos7/) *
+ [Pacote AppImage da versão noturna (64bit) do Kate](https://binary-factory.kde.org/job/Kate_Nightly_appimage-centos7/) **
+ [Compile-o](/build-it/#linux) a partir do código-fonte.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate na Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< store_badge type="msstore" link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" divClass="store-badge" imgClass="store-badge-img" >}}

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)
+ [Instalador (64bits) da versão lançada do Kate](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *
+ [Instalador (64bits) da versão noturna do Kate](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Compile-o](/build-it/#windows) a partir do código-fonte.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Instalador da versão lançada do Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Instalador da versão lançada do Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Compile-o](/build-it/#mac) a partir do código-fonte.

{{< /get-it >}}

{{< get-it-info >}}

**Acerca das versões:** <br>O [Kate](https://apps.kde.org/en/kate) e o [KWrite](https://apps.kde.org/en/kwrite) fazem parte das [Aplicações do KDE](https://apps.kde.org), que são [lançadas tipicamente 3 vezes por ano em massa](https://community.kde.org/Schedules). O [motor de edição de texto](https://api.kde.org./frameworks/ktexteditor/html/) e o [realce de sintaxe](https://api.kde.org/frameworks/syntax-highlighting/html/) são fornecidos pelas [Plataformas do KDE](https://kde.org/announcements/kde-frameworks-5.0/), que são [actualizadas mensalmente](https://community.kde.org/Schedules/Frameworks). As novas versões vão sendo anunciadas [aqui](https://kde.org/announcements/).

\* Os pacotes **release** contêm a última versão do Kate e das Plataformas do KDE.

\*\* Os pacotes **nightly** são compilados automaticamente todos os dias a partir do código-fonte, pelo que poderão estar instáveis e conter erros ou funcionalidades incompletas. Só são recomendados para fins de testes.

{{< /get-it-info >}}
