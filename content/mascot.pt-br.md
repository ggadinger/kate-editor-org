---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: Mascote do Kate
---
O mascote do Kate, Kate a pica-pau cibernética, foi desenhada por [Tyson Tan](https://www.tysontan.com/).

A evolução atual de nosso mascote foi revelada pela primeira vez [em abril de 2021](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## Kate a pica-pau cibernética

![Kate a pica-pau cibernética](/images/mascot/electrichearts_20210103_kate_normal.png)

## Versão sem texto de rodapé

![Kate a pica-pau cibernética - sem texto](/images/mascot/electrichearts_20210103_kate_notext.png)

## Versão sem fundo

![Kate a pica-pau cibernética - sem fundo](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Licenciamento

Para o licenciamento, citando Tyson:

> Eu, por meio deste, doo a nova mascote Kate, a pica-pau cibernética Pica-pau para o projeto Kate Editor. As obras de arte são duplamente licenciadas sob LGPL (ou qualquer que seja a mesmo que Kate Editor) e Creative Commons BY-SA. Desta forma, você não precisa me pedir permissão antes de usar/modificar o mascote.

## História do design da Kate a pica-pau cibernética

Perguntei ao Tyson se posso compartilhar as versões intermediárias que trocamos para mostrar a evolução e ele concordou e até escreveu alguns resumos sobre as etapas individuais. Isso inclui o mascote inicial e o ícone que se encaixa nessa evolução de design. Portanto, apenas cito o que ele escreveu sobre as diferentes etapas abaixo.

### 2014 - Kate a pica-pau

![Versão de 2014 - Kate a pica-pau](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> A versão de 2014 foi feita talvez durante o meu ponto mais baixo como artista. Fazia anos que eu não desenhava muito e estava prestes a desistir completamente. Havia algumas ideias interessantes nesta versão, a saber, os colchetes {} em seu peito e o esquema de cores inspirado no realce do código doxygen. No entanto, a execução foi muito simplista - naquela época, eu deliberadamente parei de usar qualquer técnica sofisticada para expor minhas habilidades básicas inadequadas. Nos anos seguintes, eu estaria passando por um processo árduo para reconstruir minha base de arte autodidata do zero. Estou grato que a equipe do Kate ainda manteve este em seu site por muitos anos.

### 2020 - O novo ícone do Kate

![2020 - O novo ícone do Kate](/images/mascot/history/kate-3000px.png)

> O novo ícone do Kate foi a única vez que tentei arte vetorial real. Foi nessa época que comecei a desenvolver algum senso artístico tênue. Eu não tinha experiência anterior em design de ícones, nem tido uma disciplina como proporção guiada pela matemática. Cada forma e curva foram ajustadas usando meu instinto de artista. O conceito era muito simples: é um pássaro com a forma da letra "K" e tem que ser pontiagudo. O pássaro era mais corpulento até que Christoph apontou, mais tarde eu o transformei em uma forma mais elegante.

### 2021 - Esboço do mascote

![Versão de 2021 - Esboço do novo mascote](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> Neste esboço inicial, fiquei tentado a abandonar o conceito robótico e apenas desenhar um pássaro bonito.

### 2021 - Desenho do mascote

![Versão de 2021 - Redesenho do novo mascote](/images/mascot/history/electrichearts_20210103_kate_design.png)

> O redesenho do mascote do Kate foi retomado depois que terminei de desenhar a nova imagem de apresentação para o futuro Krita 5.0. Eu era capaz de me esforçar para fazer mais cada vez que tentava uma nova imagem naquela época, então estava determinado a manter o conceito robótico. Embora a proporção estivesse errada, os elementos de design mecânicos foram mais ou menos cunhados neste.

### 2021 - Mascote final

![Versão de 2021 - Kate a pica-pau cibernética final](/images/mascot/electrichearts_20210103_kate_normal.png)

> A proporção foi ajustada depois que Christoph apontou. A pose foi ajustada para parecer mais confortável. As asas foram redesenhadas após estudar as asas de pássaros reais. Um teclado detalhado e atraente foi desenhado à mão para enfatizar o uso principal do aplicativo. Um fundo abstrato foi adicionado com um design de tipografia 2D adequado. A ideia geral é: inconfundivelmente vanguardista e artificial, mas também inconfundivelmente cheia de vida e humanidade.
