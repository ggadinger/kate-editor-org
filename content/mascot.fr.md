---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: Mascotte de Kate
---
La mascotte de Kate, Kate, le cyber pivert a été conçu par [Tyson Tan](https://www.tysontan.com/).

L'évolution actuelle de notre mascotte a été tout d'abord présentée [en Avril 2021](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## Kate, le cyber pivert

![Kate le cyber pivert](/images/mascot/electrichearts_20210103_kate_normal.png)

## Version sans texte de bas de page

![Kate le cyber pivert - Sans texte](/images/mascot/electrichearts_20210103_kate_notext.png)

## Version sans arrière plan

![Kate le cyber pivert - Sans arrière plan](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Licence

Pour les licences, pour citer Tyson :

> Par la présente, je fais don de la nouvelle mascotte, Kate le cyber pivert au projet de l'éditeur Kate. Les œuvres sont sous double licence « LGPL » (comme celle de l'éditeur Kate) et « Creative Commons BY-SA ». De cette façon, vous n'avez pas besoin de me demander la permission avant d'utiliser / modifier la mascotte.

## L'histoire de la conception de Kate, le cyber pivert

J'ai demandé à Tyson si je pouvais partager les versions intermédiaires que nous avons échangées pour présenter l'évolution. Il a accepté et a même rédigé quelques courts résumés des différentes étapes. Cela inclut la mascotte initiale et l'icône qui s'inscrivent dans cette évolution de la conception graphique. Je cite donc simplement ce qu'il a écrit sur les différentes étapes ci-dessous.

### 2014 - Kate, le cyber pivert

![Version 2014 - Kate le pivert](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> La version de 2014 a été réalisée peut-être pendant mon point le plus bas en tant qu'artiste. Je n'avais pas beaucoup dessiné depuis des années et j'étais sur le point d'abandonner complètement. Il y avait quelques idées intéressantes dans cette version, notamment les crochets {} sur sa poitrine et le schéma de couleurs inspiré de la mise en évidence du code « doxygen ». Cependant, la réalisation était très simpliste - à cette époque, j'ai délibérément arrêté d'utiliser des techniques fantaisistes pour exposer mes compétences de base insuffisantes. Dans les années qui ont suivi, je me suis lancé dans un processus difficile pour reconstruire à partir de zéro mes bases artistiques brisées et autodidactes. Je suis reconnaissant à l'équipe de Kate d'avoir conservé celle-ci sur son site Internet pendant de nombreuses années.

### 2020 - La nouvelle icône de Kate

![Version 2020  - La nouvelle icône de Kate](/images/mascot/history/kate-3000px.png)

> La nouvelle icône de Kate a été la seule fois où j'ai tenté un véritable travail d'artiste en mode vectoriel. C'est à ce moment-là que j'ai commencé à développer un vague sens artistique. Je n'avais aucune expérience préalable de la conception d'icônes. Il n'y avait aucune discipline comme les proportions guidées par les mathématiques. Chaque forme et chaque courbe ont été retravaillées en utilisant ma sensibilité d'artiste. Le concept était très simple : il s'agissait d'un oiseau ayant la forme de la lettre « K » et il devait être pointu. L'oiseau était plus grossier jusqu'à ce que Christoph me le fasse remarquer. Je l'ai ensuite transformé en une forme plus élégante.

### 2021 - Croquis de la mascotte

![Version 2021 - Esquisse de la nouvelle mascotte](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> Dans cette première esquisse, j'étais tenté d'abandonner le concept robotique et de dessiner à la place un oiseau beau et mignon.

### 2021 - Conception de la mascotte

![Version 2021 -Reprise de la conception pour la nouvelle mascotte](/images/mascot/history/electrichearts_20210103_kate_design.png)

> La refonte de la mascotte de Kate a repris après la réalisation de mon dessin de la nouvelle image d'accueil pour la version à venir de Krita 5.0. J'étais capable de me pousser à en faire plus à chaque fois que je tentais une nouvelle image à ce moment-là. Donc, j'étais déterminé à conserver le concept robotique. Bien que les proportions soient fausses, les éléments de conception du mécanisme ont plus ou moins été inventés dans ce dessin.

### 2021 - Version finale de la mascotte

![Version 2021 - Version finale pour Kate, le cyber pivert](/images/mascot/electrichearts_20210103_kate_normal.png)

> La proportion a été ajustée après les remarques de Christoph. La pose a été ajustée pour être plus confortable. Les ailes ont été redessinées après avoir étudié les ailes de vrais oiseaux. Un clavier détaillé a été dessiné à la main pour mettre en valeur l'utilisation principale de l'application. Un arrière-plan abstrait a été ajouté avec un design typographique 2D approprié. L'idée générale est la suivante : indéniablement à la pointe de la technologie et artificielle, mais aussi indéniablement pleine de vie et d'humanité.
