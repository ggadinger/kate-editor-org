---
title: Merge Requests - November 2021
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2021/11/
---

#### Week 48

- **Kate - [Add some keywords to help people find Kate and KWrite in a search](https://invent.kde.org/utilities/kate/-/merge_requests/520)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged after 2 days.

#### Week 47

- **KTextEditor - [Bring back git via QProcess](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/226)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 14 days.

- **KTextEditor - [If user didn&#x27;t set them, don&#x27;t override file type and highlighting modes detection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/230)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 3 days.

- **KSyntaxHighlighting - [Imporve current orgmode highlight support](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/276)**<br />
Request authored by [Gary Wang](https://invent.kde.org/garywang) and merged after 2 days.

- **KSyntaxHighlighting - [A couple of improvements to the ConTeXt syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/275)**<br />
Request authored by [Jack Hill](https://invent.kde.org/jackh) and merged after 7 days.

#### Week 46

- **Kate - [Introducing Colored Brackets plugin](https://invent.kde.org/utilities/kate/-/merge_requests/515)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 11 days.

- **KTextEditor - [Pass KTextEditor::Cursor by value everywhere](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/229)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **KSyntaxHighlighting - [Add syntax highlighting for OrgMode file](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/272)**<br />
Request authored by [Gary Wang](https://invent.kde.org/garywang) and merged after 2 days.

- **KSyntaxHighlighting - [Fix ConTeXt inline math command highlighting.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/273)**<br />
Request authored by [Jack Hill](https://invent.kde.org/jackh) and merged after one day.

- **KSyntaxHighlighting - [cmake.xml: Updates for CMake 3.22](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/274)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **Kate - [git-blame: Fix commit summary not available](https://invent.kde.org/utilities/kate/-/merge_requests/518)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

#### Week 45

- **KTextEditor - [Revert &quot;completion: invoke always&quot; and honor min word length for completion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/224)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [Remove selected text if preedit is not empty.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/225)**<br />
Request authored by [Xuetian Weng](https://invent.kde.org/xuetianweng) and merged at creation day.

- **Kate - [Add more options for clangd](https://invent.kde.org/utilities/kate/-/merge_requests/517)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [add racket lsp](https://invent.kde.org/utilities/kate/-/merge_requests/516)**<br />
Request authored by [shenleban tongying](https://invent.kde.org/slbtongying) and merged after 2 days.

- **KTextEditor - [Fix vi-mode completion unit-tests](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/223)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 2 days.

- **KSyntaxHighlighting - [add racket.xml](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/271)**<br />
Request authored by [shenleban tongying](https://invent.kde.org/slbtongying) and merged after one day.

- **Kate - [FileTreePlugin: setTooltip instead of setText for icon only toolbar](https://invent.kde.org/utilities/kate/-/merge_requests/514)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Improve built-in search performance](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/219)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

#### Week 44

- **Kate - [LSP Completion: use textEdit.newText as a workaround](https://invent.kde.org/utilities/kate/-/merge_requests/513)**<br />
Request authored by [oldherl oh](https://invent.kde.org/oldherl) and merged at creation day.

- **Kate - [Fix compile cpp when path has spaces](https://invent.kde.org/utilities/kate/-/merge_requests/512)**<br />
Request authored by [Thiago Sueto](https://invent.kde.org/thiagosueto) and merged after one day.

- **KTextEditor - [Remove duplicated lua.js indent file](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/222)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix assert condidtion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/220)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [Fix a compiler warning](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/221)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [Re-add &quot;undo removed tail&quot; on completion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/208)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 12 days.

- **KTextEditor - [Review KateTextLine usages](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/218)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Create a separate path for fetching lineLength](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/214)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [Use iterators for iterating over blocks](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/217)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix find_package call](https://invent.kde.org/utilities/kate/-/merge_requests/511)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **KTextEditor - [ViMode: Dont respond to doc changes when vi mode is disabled for view](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/215)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Expand katepart metadata](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/213)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after 3 days.

- **Kate - [Git: Maintain a recent push/pull command history](https://invent.kde.org/utilities/kate/-/merge_requests/510)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

