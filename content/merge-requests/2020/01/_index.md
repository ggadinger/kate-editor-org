---
title: Merge Requests - January 2020
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2020/01/
---

#### Week 04

- **Kate - [ColorSchemeChooser improvements](https://invent.kde.org/utilities/kate/-/merge_requests/59)**<br />
Request authored by [David Redondo](https://invent.kde.org/davidre) and merged after 4 days.

- **Kate - [Fix External Tool &quot;Google Selected Text&quot;](https://invent.kde.org/utilities/kate/-/merge_requests/58)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [External tools: Correctly set the working directory](https://invent.kde.org/utilities/kate/-/merge_requests/57)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after 2 days.

#### Week 03

- **Kate - [doc: extended lspclient documentation](https://invent.kde.org/utilities/kate/-/merge_requests/56)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

#### Week 02

- **Kate - [Highlight documentation: add equivalence in regex for number rules and add lookbehind assertions](https://invent.kde.org/utilities/kate/-/merge_requests/55)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

