---
title: Merge Requests - June 2020
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2020/06/
---

#### Week 26

- **KTextEditor - [Make &quot;goto line&quot; work backwards](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/4)**<br />
Request authored by [Nazar Kalinowski](https://invent.kde.org/nazark) and merged after one day.

- **Kate - [Use QTabBar](https://invent.kde.org/utilities/kate/-/merge_requests/89)**<br />
Request authored by [Tomaz  Canabrava](https://invent.kde.org/tcanabrava) and merged after one day.

- **KSyntaxHighlighting - [Add YARA language](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/4)**<br />
Request authored by [Wes H](https://invent.kde.org/hurdw) and merged after 32 days.

- **KSyntaxHighlighting - [Add Snort/Suricata language highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/5)**<br />
Request authored by [Wes H](https://invent.kde.org/hurdw) and merged after 27 days.

#### Week 25

- **Kate - [build-plugin: also accept + in message filename detection](https://invent.kde.org/utilities/kate/-/merge_requests/88)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 3 days.

#### Week 24

- **Kate - [session: sort list by ascending name](https://invent.kde.org/utilities/kate/-/merge_requests/87)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KSyntaxHighlighting - [no deprecated binary json](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/8)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [JavaScript/TypeScript: highlight tags in templates](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/6)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after one day.

