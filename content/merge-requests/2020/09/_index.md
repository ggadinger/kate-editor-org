---
title: Merge Requests - September 2020
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
url: /merge-requests/2020/09/
---

#### Week 40

- **Kate - [add open-with entry to the context menu of the filebrowser](https://invent.kde.org/utilities/kate/-/merge_requests/110)**<br />
Request authored by [Mario Aichinger](https://invent.kde.org/aichingm) and merged at creation day.

- **Kate - [Add settings to toggle tabs close button and expansion](https://invent.kde.org/utilities/kate/-/merge_requests/109)**<br />
Request authored by [George Florea Bănuș](https://invent.kde.org/georgefb) and merged at creation day.

- **Kate - [addons/externaltools: Really respect BUILD_TESTING](https://invent.kde.org/utilities/kate/-/merge_requests/108)**<br />
Request authored by [Andreas Sturmlechner](https://invent.kde.org/asturmlechner) and merged at creation day.

- **Kate - [externaltools: extend git blame command line](https://invent.kde.org/utilities/kate/-/merge_requests/107)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KTextEditor - [[kateprinter] Portaway from deprecated QPrinter methods](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/31)**<br />
Request authored by [Ömer Fadıl Usta](https://invent.kde.org/usta) and merged after one day.

- **Kate - [improve Kate config dialog](https://invent.kde.org/utilities/kate/-/merge_requests/105)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Use colorful &quot;behavior&quot; icon in settings window sidebar](https://invent.kde.org/utilities/kate/-/merge_requests/106)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged at creation day.

- **KSyntaxHighlighting - [Fix doxygen latex formulas](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/72)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

#### Week 39

- **Kate - [Port KRun to new KIO classes](https://invent.kde.org/utilities/kate/-/merge_requests/104)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **KTextEditor - [Don&#x27;t create temporary buffer to detect mimetype for saved local file](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/30)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff) and merged at creation day.

- **KSyntaxHighlighting - [ANSI highlighter](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/70)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 8 days.

- **KSyntaxHighlighting - [Add Radical color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/68)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 5 days.

- **KSyntaxHighlighting - [make separator color less intrusive](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/71)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

#### Week 38

- **KTextEditor - [[Vimode]Improve presentation of unset buffers on error](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/27)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 2 days.

- **KTextEditor - [move separator from between icon border and line numbers to between bar and text](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/28)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Add Nord color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/67)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [add proper license information to all themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/69)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [gruvbox theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/66)**<br />
Request authored by [Frederik Banning](https://invent.kde.org/laubblaeser) and merged after 2 days.

- **Kate - [Update configuration docs](https://invent.kde.org/utilities/kate/-/merge_requests/103)**<br />
Request authored by [Yuri Chornoivan](https://invent.kde.org/yurchor) and merged at creation day.

- **KTextEditor - [[Vimode]Make the behavior of numbered registers more accurate](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/26)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [Restore behavior of find selected when no selection is available](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/25)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff) and merged after one day.

- **Kate - [implement optional limit for number of tabs and LRU replacement](https://invent.kde.org/utilities/kate/-/merge_requests/101)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Bug:426451](https://invent.kde.org/utilities/kate/-/merge_requests/102)**<br />
Request authored by [Ayushmaan jangid](https://invent.kde.org/ayushmaanjangid) and merged at creation day.

- **KTextEditor - [Clean up TwoViewCursor, open/close distinction has been redundant since 2007](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/24)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [Add ayu color theme (with light, dark and mirage variants)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/65)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Add POSIX/gmake alternate for Makefile simple variable assignment](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/64)**<br />
Request authored by [Peter Mello](https://invent.kde.org/pmello) and merged after one day.

#### Week 37

- **KTextEditor - [Completely switch over to KSyntaxHighlighting::Theme usage](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/21)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 6 days.

- **KSyntaxHighlighting - [tools to generate a graph from a syntax file](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/63)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Avoid some unneeded clones of snippet repositories.](https://invent.kde.org/utilities/kate/-/merge_requests/99)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after 2 days.

- **Kate - [Fix cancel button being focused](https://invent.kde.org/utilities/kate/-/merge_requests/100)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **KTextEditor - [add shortcut for paste mouse selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/23)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Move DocumentPrivate::attributeAt to KateViewInternal::attributeAt](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/22)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [Add unit tests for Alerts and Modelines](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/59)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Update README](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/61)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

#### Week 36

- **KTextEditor - [Use KSyntaxHighlighting::Theme infrastructure for color themes in KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/20)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 11 days.

- **KSyntaxHighlighting - [Add Debian changelog and control example files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/57)**<br />
Request authored by [Samuel Gaist](https://invent.kde.org/sgaist) and merged at creation day.

- **KSyntaxHighlighting - [add the &#x27;Dracula&#x27; color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/58)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [change theme json format, use meta object enum names for editor colors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/54)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [check kateversion &gt;= 5.62 for fallthroughContext without fallthrough=&quot;true&quot;...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/56)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Add syntax definition for todo.txt](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/55)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Fix Int, Float and HlC* rules](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/53)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 4 days.

- **Kate - [Drop empty X-KDE-PluginInfo-Depends](https://invent.kde.org/utilities/kate/-/merge_requests/98)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

